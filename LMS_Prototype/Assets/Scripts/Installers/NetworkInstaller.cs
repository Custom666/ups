﻿using LMS_Core.Infrastructure;
using LMS_Core.Managers;
using LMS_Core.Models;
using LMS_Core.Services;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{
    public class NetworkInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IService>().To<NetworkService>().AsSingle();

            Container.Bind<IManager<NetworkClient>>().To<ClientManager>().AsSingle();
            Container.Bind<IManager<Lobby>>().To<LobbyManager>().AsSingle();
            Container.Bind<IManager<Game>>().To<GameManager>().AsSingle();
            Container.Bind<IManager<Error>>().To<ErrorManager>().AsSingle();
        }
    }
}
