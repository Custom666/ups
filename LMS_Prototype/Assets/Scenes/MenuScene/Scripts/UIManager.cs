﻿using System.Linq;
using LMS_Core;
using System.Net;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using LMS_Core.Infrastructure;
using LMS_Core.Enums;
using LMS_Core.Models;
using Assets.Scripts.Menu.Controllers;
using Assets.Scripts.Menu.ViewModels;

namespace Assets.Scenes.MenuScene.Scripts
{
    public class UIManager : MonoBehaviour
    {
        private void Start()
        {
            loadSettings();
        }

        private void loadSettings()
        {
            IPAddress ip;

            GameSettings.PlayerName = string.IsNullOrEmpty(PlayerPrefs.GetString("playerName")) ? "Unknown" : PlayerPrefs.GetString("playerName");
            GameSettings.IpAddress = IPAddress.TryParse(PlayerPrefs.GetString("ipAdress"), out ip) ? ip : IPAddress.Parse("127.0.0.1");
            GameSettings.Port = PlayerPrefs.GetInt("port") <= 0 ? 10000 : PlayerPrefs.GetInt("port");
        }

        public void OpenSettings()
        {
            GameObject.Find("PlayerNameInputField").GetComponentInChildren<Text>().text = GameSettings.PlayerName;
            GameObject.Find("IPAddressInputField").GetComponentInChildren<Text>().text = GameSettings.IpAddress.ToString();
            GameObject.Find("PortInputField").GetComponentInChildren<Text>().text = GameSettings.Port.ToString();
        }

        public void SaveSettings()
        {
            var playerName = GameObject.Find("PlayerNameInputField").GetComponentsInChildren<Text>()[1].text;
            var port = GameObject.Find("PortInputField").GetComponentsInChildren<Text>()[1].text;
            var ipAddress = GameObject.Find("IPAddressInputField").GetComponentsInChildren<Text>()[1].text;

            if (!string.IsNullOrEmpty(playerName)) GameSettings.PlayerName = playerName;
            if (!string.IsNullOrEmpty(port)) GameSettings.Port = int.Parse(port);
            if (!string.IsNullOrEmpty(ipAddress)) GameSettings.IpAddress = IPAddress.Parse(ipAddress);

            PlayerPrefs.SetString("playerName", GameSettings.PlayerName);
            PlayerPrefs.SetInt("port", GameSettings.Port);
            PlayerPrefs.SetString("ipAdress", GameSettings.IpAddress.ToString());

            PlayerPrefs.Save();
        }

        public void LoadScene(string scene)
        {
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
        }

        public void JoinLobby(Lobby model)
        {
            var multiplayerScreenController = transform.GetComponentInChildren<MultiplayerScreenController>();
            var lobbyScreenController = Resources.FindObjectsOfTypeAll<LobbyScreenController>()[0];

            multiplayerScreenController.gameObject.SetActive(false);

            lobbyScreenController.Model = model;
            lobbyScreenController.gameObject.SetActive(true);
        }

        public void LeaveLobby()
        {
            var multiplayerScreenController = Resources.FindObjectsOfTypeAll<MultiplayerScreenController>()[0];
            var lobbyScreenController = transform.GetComponentInChildren<LobbyScreenController>();

            lobbyScreenController.gameObject.SetActive(false);

            multiplayerScreenController.gameObject.SetActive(true);
            multiplayerScreenController.gameObject.transform.Find("DestroyLobbyInfoDialog").gameObject.SetActive(true);
        }
    }
}
