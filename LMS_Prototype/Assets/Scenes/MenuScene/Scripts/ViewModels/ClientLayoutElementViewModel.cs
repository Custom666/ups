﻿using Assets.Scenes.MenuScene.Scripts.Controllers;
using LMS_Core.Models;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Menu.ViewModels
{
    public class ClientLayoutElementViewModel : MonoBehaviour
    {
        private Text _clientName;

        public NetworkClient Model { get; set; }

        private void Awake()
        {
            _clientName = transform.GetComponentsInChildren<Text>().First(t => t.name == "ClientName");
        }

        private void Start()
        {
            if (Model == null) return;

            _clientName.text = Model.Name;
        }
    }
}