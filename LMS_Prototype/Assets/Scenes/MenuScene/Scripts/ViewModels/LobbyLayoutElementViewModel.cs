﻿using Assets.Scenes.MenuScene.Scripts;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Menu.ViewModels
{
    public class LobbyLayoutElementViewModel : MonoBehaviour
    {
        public IManager<Lobby> Manager { get; set; }

        private Button _joinLobbyButton;
        private Text _lobbyName;
        
        public Lobby Model { get; set; }

        private bool _canOpenLobby;

        private void Awake()
        {
            _lobbyName = transform.GetComponentsInChildren<Text>().First(t => t.name == "LobbyName");
            _joinLobbyButton = transform.GetComponentsInChildren<Button>().First(t => t.name == "JoinLobbyButton");
        }

        private void ModelClients_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) foreach (NetworkClient client in e.NewItems) if (client.Equals(LMS_Core.GameSettings.Player)) _canOpenLobby = true;
        }

        private void Start()
        {
            Model.Clients.CollectionChanged += ModelClients_CollectionChanged;

            _lobbyName.text = Model.Name;

            _joinLobbyButton.onClick.AddListener(onOpenLobbyButtonClick);
        }

        private void LateUpdate()
        {
            if (_canOpenLobby)
            {
                _canOpenLobby = false;

                var lobby = Manager.Items.FirstOrDefault(l => l.Name == Model.Name);

                GameObject.Find("UI").GetComponent<UIManager>().JoinLobby(lobby);
            }
        }

        private void onOpenLobbyButtonClick()
        {
            if (Model == null) return;

            Manager.Set(Model, LMS_Core.Enums.RequestType.Join);
        }
    }
}