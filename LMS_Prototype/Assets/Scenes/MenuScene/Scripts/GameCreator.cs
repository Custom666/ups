﻿using Assets.Scripts.Menu;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scenes.MenuScene.Scripts
{
    public class GameCreator : MonoBehaviour
    {
        [Inject] private IManager<Game> _manager;
        private bool _canLoadGameScene;

        private void OnEnable()
        {
            _canLoadGameScene = false;
            _manager.Items.CollectionChanged += Items_CollectionChanged;
        }

        private void OnDisable()
        {
            _manager.Items.CollectionChanged -= Items_CollectionChanged;
        }
        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && e.NewItems.Count > 0) _canLoadGameScene = true;
        }

        private void LateUpdate()
        {
            if (_canLoadGameScene)
            {
                var lobbyScreenController = Resources.FindObjectsOfTypeAll<Assets.Scripts.Menu.Controllers.LobbyScreenController>()[0];
                var multiplayerScreenController = Resources.FindObjectsOfTypeAll<Assets.Scripts.Menu.Controllers.MultiplayerScreenController>()[0];

                lobbyScreenController.gameObject.SetActive(false);
                multiplayerScreenController.gameObject.SetActive(true);

                SceneManager.LoadScene("GameScene");
            }
        }
    }
}
