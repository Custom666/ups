﻿using Assets.Scenes.MenuScene.Scripts;
using Assets.Scripts.Menu.ViewModels;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using System;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.Menu.Controllers
{
    public class LobbyScreenController : MonoBehaviour
    {
        [SerializeField] private GameObject _createGameButton;
        [SerializeField] private GameObject _clientLayoutElementPrefab;
        [SerializeField] private Transform _scrollViewContent;

        [SerializeField] private Text _lobbyNameText;
        [SerializeField] private Text _ownerNameText;
        [SerializeField] private Text _capacityText;

        [Inject] private IService _network;
        [Inject] private IManager<Lobby> _manager;

        public Lobby Model { get; set; }

        private bool _isDirty;
        private bool _isDestroyed;

        private void Awake()
        {
            _manager.Items.CollectionChanged += Items_CollectionChanged;
        }

        private void OnDestroy()
        {
            _manager.Items.CollectionChanged -= Items_CollectionChanged;
        }

        private void OnEnable()
        {
            _createGameButton.SetActive(Model.Owner.Name.CompareTo(LMS_Core.GameSettings.Player.Name) == 0);

            _lobbyNameText.text = Model.Name;
            _ownerNameText.text = Model.Owner.Name;
            _capacityText.text = Model.Capacity.ToString();

            _isDestroyed = false;

            _isDirty = true;

            Model.Clients.CollectionChanged += Clients_CollectionChanged;
        }

        private void OnDisable()
        {
            Model.Clients.CollectionChanged -= Clients_CollectionChanged;
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // model was removed => leave this screen
            if (e.OldItems != null) foreach (Lobby model in e.OldItems) if (model.Id == Model.Id) _isDestroyed = true;
        }

        private void Clients_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _isDirty = true;
        }

        private void LateUpdate()
        {
            if (_isDestroyed) // leave lobby
            {
                GameObject.Find("UI").GetComponent<UIManager>().LeaveLobby();
            }
            else if (_isDirty) // synchronize
            {
                foreach (Transform child in _scrollViewContent) Destroy(child.gameObject);

                foreach (var client in Model.Clients)
                {
                    if (client.Id == LMS_Core.GameSettings.Player.Id) continue; // do not display current client

                    var clientGameObject = Instantiate(_clientLayoutElementPrefab, _scrollViewContent);

                    clientGameObject.GetComponent<ClientLayoutElementViewModel>().Model = client;
                }

                _isDirty = false;
            }
        }

        public void LeaveLobby()
        {
            _manager.Set(Model, LMS_Core.Enums.RequestType.Leave);
        }

        public void StartGame()
        {
            try
            {
                _network.Send("GC" + Model.Id);
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);

                return;
                //TODO validation 
            }

            SceneManager.LoadScene("GameScene");
        }
    }
}
