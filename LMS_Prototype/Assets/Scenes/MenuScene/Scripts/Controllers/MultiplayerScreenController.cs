﻿using Assets.Scripts.Menu.ViewModels;
using LMS_Core;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using LMS_Core.Services;
using System;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using System.Linq;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Menu.Controllers
{
    public class MultiplayerScreenController : MonoBehaviour
    {
        [SerializeField] private Button _openCreateLobbyDialogButton;
        [SerializeField] private Text _connectionStatus;
        [SerializeField] private Text _connectionStatusInformation;
        [SerializeField] private Transform _scrollViewContent;
        [SerializeField] private GameObject _lobbyPrefab;
        [SerializeField] private float _connectionInterval = 5f;

        [Inject] private IService _network;
        [Inject] private IManager<Lobby> _lobbyManager;
        [Inject] private IManager<NetworkClient> _clientManager;
        [Inject] private IManager<Error> _errorManager;
        [Inject] private IManager<Game> _gameManager;

        private bool _authenticated => GameSettings.Player != null && GameSettings.Player.Id > 0;

        private bool _isDirty = false;
        private bool _synchronized;
        private bool _authenticating;
        private float _lastConnectionCheck;
        private string _connectionStatusInformationText;
        private bool _hasError;
        private Game _existGame;

        private void Awake()
        {
            _gameManager.Items.CollectionChanged += GameItems_CollectionChanged;
            _lobbyManager.Items.CollectionChanged += LobbyItems_CollectionChanged;
            _errorManager.Items.CollectionChanged += ErrorItems_CollectionChanged;
        }

        private void GameItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) foreach (Game g in e.NewItems) if (g.Clients.Contains(GameSettings.Player)) _existGame = g;
            if (e.OldItems != null) foreach (Game g in e.OldItems) if (g.Clients.Contains(GameSettings.Player)) _existGame = null;
        }

        private void ErrorItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems == null || e.NewItems.Count <= 0) return;

            foreach (Error error in e.NewItems)
            {
                if (error.EntityType == LMS_Core.Enums.EntityType.Unknown) continue;

                _connectionStatusInformationText = error.Message;
                _hasError = true;
            }
        }

        private void LobbyItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _isDirty = true;
        }

        private void OnDestroy()
        {
            _lobbyManager.Items.CollectionChanged -= LobbyItems_CollectionChanged;
            _errorManager.Items.CollectionChanged -= ErrorItems_CollectionChanged;
        }

        private void OnEnable()
        {
            _connectionStatusInformationText = string.Empty;

            _hasError = _isDirty = true;

            _synchronized = false;
        }
        
        private void NetworkManager_OnLobbyItemsChangedEvent()
        {
            _isDirty = true;
        }

        private void LateUpdate()
        {
            _openCreateLobbyDialogButton.gameObject.SetActive(_network.ServiceStatus == LMS_Core.Enums.ServiceStatus.Authenticated);

            _connectionStatus.text = _network.ServiceStatus.ToString();

            if (_hasError)
            {
                _connectionStatusInformation.text = _connectionStatusInformationText;

                _hasError = false;
            }

            switch (_network.ServiceStatus)
            {
                case LMS_Core.Enums.ServiceStatus.Disconnected:

                    if (Time.time - _lastConnectionCheck > _connectionInterval)
                    {
                        connect();

                        _lastConnectionCheck = Time.time;
                    }

                    break;

                case LMS_Core.Enums.ServiceStatus.Connected:

                    if (!_authenticated && (Time.time - _lastConnectionCheck > _connectionInterval))
                    {
                        _network.Authenticate();

                        _lastConnectionCheck = Time.time;
                    }

                    break;

                case LMS_Core.Enums.ServiceStatus.Authenticated:

                    if (!_synchronized)
                    {
                        _synchronized = true;

                        _network.Send("CA");
                        _network.Send("LA");
                        _network.Send("GA");
                    }

                    if (_existGame != null && GameSettings.Player.Health > 0)
                    {
                        _existGame = null;
                        SceneManager.LoadScene("GameScene");
                    }

                    if (_isDirty)
                    {
                        foreach (Transform child in _scrollViewContent) Destroy(child.gameObject);

                        foreach (var item in _lobbyManager.Items)
                        {
                            var lobbyGameObject = Instantiate(_lobbyPrefab, _scrollViewContent);

                            lobbyGameObject.GetComponent<LobbyLayoutElementViewModel>().Model = item;
                            lobbyGameObject.GetComponent<LobbyLayoutElementViewModel>().Manager = _lobbyManager;
                        }

                        _isDirty = false;
                    }

                    break;
            }
        }

        public void Disconnect()
        {
            _lobbyManager.Items.Clear();
            _clientManager.Items.Clear();

            _network.Disconnect();
        }

        private async void connect()
        {
            try
            {
                await _network.ConnectAsync($"{GameSettings.IpAddress};{GameSettings.Port}");
            }
            catch (Exception e)
            {
                _connectionStatusInformationText = e.Message;
                _hasError = true;
                return;
            }
        }
    }
}
