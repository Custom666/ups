﻿using Assets.Scripts.Menu;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scenes.MenuScene.Scripts.Controllers
{
    public class CreateLobbyDialogController : MonoBehaviour
    {
        [SerializeField] private Text _statusInformationText;
        [SerializeField] private InputField _inputField;
        [SerializeField] private Button _okButton;

        [Inject] private IService _network;
        [Inject] private IManager<Lobby> _manager;

        private Lobby _newLobby;
        private bool _canOpenLobby;

        private void Awake()
        {
            _okButton.onClick.AddListener(onOkButtonClick);
        }

        private void OnEnable()
        {
            _canOpenLobby = false;

            _manager.Items.CollectionChanged += Items_CollectionChanged;
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) foreach (Lobby item in e.NewItems) if (_newLobby != null && item.Name == _newLobby.Name) { _newLobby = item; _canOpenLobby = true; }
        }

        private void onOkButtonClick()
        {
            if (LMS_Core.GameSettings.Player == null)
            {
                _statusInformationText.text = "You have to be authenticated for this operation!";
            }
            else if (string.IsNullOrEmpty(_inputField.text))
            {
                _statusInformationText.text = "Lobby name cannot be empty!";
            }
            else if (_inputField.text.Length > LMS_Core.Managers.LobbyManager.MAX_LOBBY_NAME_LENGTH)
            {
                _statusInformationText.text = $"Lobby name cannot longer then { LMS_Core.Managers.LobbyManager.MAX_LOBBY_NAME_LENGTH } characters!";
            }
            else
            {
                _newLobby = new Lobby(_inputField.text, 5, LMS_Core.GameSettings.Player);

                _manager.Set(_newLobby, LMS_Core.Enums.RequestType.Create);
            }
        }

        private void LateUpdate()
        {
            if (_canOpenLobby)
            {
                _canOpenLobby = false;

                gameObject.SetActive(false);

                GameObject.Find("UI").GetComponent<UIManager>().JoinLobby(_newLobby);
            }
        }
    }
}
