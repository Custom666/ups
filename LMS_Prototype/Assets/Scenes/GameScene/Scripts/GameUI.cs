﻿using LMS_Core.Infrastructure;
using LMS_Core.Models;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scenes.GameScene.Scripts
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private GameObject _menu;

        [Inject] private IService _service;
        [Inject] private IManager<Error> _errorManager;
        [Inject] private IManager<Game> _gameManager;

        private Text _connectionStatusTest;
        private Text _positionText;
        private Text _forwardText;
        private Text _enemyPositionText;
        private Text _errorText;
        private string _error;
        private bool _disconnected;

        private void Awake()
        {
            _connectionStatusTest = GameObject.Find("ConnectionStatus").gameObject.GetComponent<Text>();
            _positionText = GameObject.Find("Position").gameObject.GetComponent<Text>();
            _forwardText = GameObject.Find("Forward").gameObject.GetComponent<Text>();
            _enemyPositionText = GameObject.Find("EnemyPosition").gameObject.GetComponent<Text>();
            _errorText = GameObject.Find("Error").gameObject.GetComponent<Text>();
        }

        private void OnEnable()
        {
            _disconnected = false;

            LMS_Core.Services.NetworkService.OnServiceStatusChangedEvent += NetworkService_OnServiceStatusChangedEvent;
            _errorManager.Items.CollectionChanged += ErrorItems_CollectionChanged;
        }
        
        private void OnDisable()
        {
            LMS_Core.Services.NetworkService.OnServiceStatusChangedEvent -= NetworkService_OnServiceStatusChangedEvent;
            _errorManager.Items.CollectionChanged -= ErrorItems_CollectionChanged;
        }
        private void NetworkService_OnServiceStatusChangedEvent()
        {
            if (_service.ServiceStatus == LMS_Core.Enums.ServiceStatus.Disconnected) _disconnected = true;
        }

        private void ErrorItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (Error error in e.NewItems)
                {
                    if (error.EntityType != LMS_Core.Enums.EntityType.Unknown) _error = error.Message;
                }
            }
        }

        private void LateUpdate()
        {
            if (_disconnected) SceneManager.LoadScene("MenuScene");

            if (Input.GetKeyUp(KeyCode.Escape)) _menu.SetActive(!_menu.activeInHierarchy);

            _connectionStatusTest.text = _service.ServiceStatus.ToString();

            if (!string.IsNullOrEmpty(_error)) { _errorText.text = _error; _error = string.Empty; }

            _positionText.text = $"x: { (int)LMS_Core.GameSettings.Player.PositionX} y: { (int)LMS_Core.GameSettings.Player.PositionY} z: { (int)LMS_Core.GameSettings.Player.PositionZ}";
            _forwardText.text = $"y-rotation: { LMS_Core.GameSettings.Player.RotationY }";

            _enemyPositionText.text = string.Empty;

            if(_gameManager != null && _gameManager.Items.Count > 0) foreach (var e in _gameManager.Items[0].Clients) _enemyPositionText.text += $"{ e.Name } x: { (int)e.PositionX} y: { (int)e.PositionY} z: { (int)e.PositionZ} ry: { (int)e.RotationY }\n";
        }

        public void BackToMenu()
        {
            SceneManager.LoadScene("MenuScene");
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}
