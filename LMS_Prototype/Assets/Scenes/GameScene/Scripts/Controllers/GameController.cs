﻿using Assets.Player.Scripts;
using LMS_Core;
using LMS_Core.Infrastructure;
using LMS_Core.Managers;
using LMS_Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scenes.GameScene.Scripts.Controllers
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private GameObject _enemyPrefab;
        [SerializeField] private GameObject _endGameUI;
        [SerializeField] private Text _endGameText;
        [SerializeField] private float _threshold = 1;

        [Inject] private IService _service;
        [Inject] private IManager<Game> _gameManager;
        [Inject] private IManager<NetworkClient> _clientManager;

        private ObservableCollection<GameObject> _enemies;

        private bool _canUpdate;
        private float _lastHealth;
        private Vector3 _lastPosition;
        private float _lastRotation;
        private NetworkClient _enemyToDestroy;
        private NetworkClient _enemyToCreate;
        private bool _isDirty;

        private void Awake()
        {
            _endGameUI.SetActive(false);

            _enemies = _enemies ?? new ObservableCollection<GameObject>();

            foreach (var enemy in _enemies) Destroy(enemy, 0);

            foreach (var client in _gameManager.Items[0].Clients)
            {
                if (client.Id == GameSettings.Player.Id || !client.IsConnected) continue;

                createEnemy(client);
            }
        }

        private void Start()
        {
            _lastPosition = new Vector3(GameSettings.Player.PositionX, GameSettings.Player.PositionY, GameSettings.Player.PositionZ);
        }

        private void OnEnable()
        {
            _gameManager.Items[0].Clients.CollectionChanged += GameClientItems_CollectionChanged;
            ClientManager.OnClindDisconnectedEvent += ClientManager_OnClindDisconnectedEvent;
            ClientManager.OnClindConnectedEvent += ClientManager_OnClindConnectedEvent;
        }

        private void OnDisable()
        {
            //_gameManager.Items[0].Clients.CollectionChanged -= GameClientItems_CollectionChanged;
            ClientManager.OnClindDisconnectedEvent -= ClientManager_OnClindDisconnectedEvent;
            ClientManager.OnClindConnectedEvent -= ClientManager_OnClindConnectedEvent;
        }

        private void createEnemy(NetworkClient client)
        {
            if (_enemies.Any(e => e != null && e.GetComponent<EnemyController>().Model.Name.CompareTo(client.Name) == 0))
            {
                _enemyToCreate = null;
                return;
            }

            var enemy = Instantiate(_enemyPrefab,
                    new Vector3(client.PositionX, client.PositionY, client.PositionZ),
                    Quaternion.Euler(0, client.RotationY, 0));

            enemy.GetComponent<EnemyController>().Model = client;

            _enemies.Add(enemy);
        }

        private void GameClientItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            _isDirty = true;
        }

        private void ClientManager_OnClindConnectedEvent(NetworkClient client)
        {
            if (GameSettings.Player.Name.CompareTo(client.Name) != 0 && _gameManager.Items[0].Clients.Contains(client)) _enemyToCreate = client;
        }

        private void ClientManager_OnClindDisconnectedEvent(NetworkClient client)
        {
            if (GameSettings.Player.Name.CompareTo(client.Name) != 0 && _gameManager.Items[0].Clients.Contains(client)) _enemyToDestroy = client;
            //foreach (var enemy in _enemies) if (enemy != null && enemy.GetComponent<EnemyController>().Model.Name.CompareTo(client.Name) == 0) _enemyToDestroy = enemy;
        }

        private void Update()
        {
            if (_endGameUI.activeInHierarchy) return; // end game

            // lost
            if (GameSettings.Player.Health <= 0)
            {
                try
                {
                    _service.Send("GD");
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                    Debug.LogError(e.StackTrace);
                }

                _endGameUI.SetActive(true);
                _endGameText.text = "YOU LOST!";
            }
            // win
            else if (_enemies.Count(e => e != null && e.GetComponent<EnemyController>().Model.Health > 0) < 1)
            {
                try
                {
                    _service.Send("GD");
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                    Debug.LogError(e.StackTrace);
                }

                _endGameUI.SetActive(true);
                _endGameText.text = "YOU WIN!";
            }
            else
            {
                #region Synchronize enemies

                if (_enemyToCreate != null)
                {
                    createEnemy(_enemyToCreate);
                    _enemyToCreate = null;
                }

                if (_enemyToDestroy != null)
                {
                    GameObject e = null;

                    foreach (var enemy in _enemies) if (enemy != null && enemy.GetComponent<EnemyController>().Model.Name.CompareTo(_enemyToDestroy.Name) == 0) e = enemy;

                    if (e != null)
                    {
                        _enemies.Remove(e);
                        Destroy(e);
                    }

                    _enemyToDestroy = null;
                }

                if (_isDirty)
                {
                    _isDirty = false;

                    var enemiesToDestroy = new List<GameObject>();

                    foreach (var enemy in _enemies)
                    {
                        var model = enemy.GetComponent<EnemyController>().Model;

                        if (!_gameManager.Items[0].Clients.Any(client => model.Equals(client))) enemiesToDestroy.Add(enemy);
                    }

                    foreach (var enemy in enemiesToDestroy)
                    {
                        _enemies.Remove(enemy);
                        Destroy(enemy);
                    }
                }

                #endregion

                // Synchronize client
                var position = new Vector3(GameSettings.Player.PositionX, GameSettings.Player.PositionY, GameSettings.Player.PositionZ);

                if (Vector3.Distance(_lastPosition, position) > _threshold
                    || Mathf.Abs(_lastRotation - GameSettings.Player.RotationY) > _threshold
                    || Math.Abs(_lastHealth - GameSettings.Player.Health) > _threshold)
                {
                    _gameManager.Set(_gameManager.Items[0], LMS_Core.Enums.RequestType.Synchronize);

                    _lastHealth = GameSettings.Player.Health;
                    _lastPosition = position;
                    _lastRotation = GameSettings.Player.RotationY;
                }

            }
        }
    }
}
