﻿using Complete;
using LMS_Core.Models;
using UnityEngine;

namespace Assets.Player.Scripts
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float _speed = 12f;
        [SerializeField] private Rigidbody _bullet;
        [SerializeField] private Transform _fireTransform;

        public NetworkClient Model { get; set; }

        private TankHealth _health;
        private bool _fired;
        private Vector3 _fireVelocity;

        private void Awake()
        {
            _health = GetComponent<TankHealth>();
        }

        private void OnEnable()
        {
            LMS_Core.Managers.GameManager.OnClientFireEvent += GameManager_OnClientFireEvent;
        }

        private void OnDisable()
        {
            LMS_Core.Managers.GameManager.OnClientFireEvent -= GameManager_OnClientFireEvent;
        }

        private void GameManager_OnClientFireEvent(NetworkClient client, float velX, float velY, float velZ)
        {
            if (client != null && Model != null && client.Equals(Model))
            {
                _fired = true;
                _fireVelocity = new Vector3(velX, velY, velZ);
            }
        }

        private void Start()
        {
            _health.m_CurrentHealth = Model.Health;
        }

        private void Update()
        {
            if (!Model.IsConnected || Model.Health <= 0) Destroy(gameObject);

            if (_fired) fire();

            transform.position = Vector3.Lerp
                (
                    transform.position,
                    new Vector3(Model.PositionX, transform.position.y, Model.PositionZ),
                    Time.deltaTime * _speed
                );

            transform.rotation = Quaternion.Lerp
                (
                    transform.rotation,
                    Quaternion.Euler(0, Model.RotationY, 0),
                    Time.deltaTime * _speed
                );
        }

        private void LateUpdate()
        {
            _health.m_CurrentHealth = Model.Health;
        }

        private void fire()
        {
            _fired = false;

            Rigidbody shellInstance = Instantiate(_bullet, _fireTransform.position, _fireTransform.rotation) as Rigidbody;

            shellInstance.velocity = _fireVelocity;
        }
    }
}
