﻿using Complete;
using LMS_Core;
using UnityEngine;

namespace Assets.Player.Scripts
{
    [RequireComponent(typeof(TankHealth))]
    public class PlayerController : MonoBehaviour
    {
        private TankHealth _health;

        private void Awake()
        {
            _health = GetComponent<TankHealth>();

            transform.position = new Vector3(GameSettings.Player.PositionX, GameSettings.Player.PositionY, GameSettings.Player.PositionZ);
            transform.Rotate(Vector3.up, GameSettings.Player.RotationY, Space.Self);
        }

        private void Update()
        {
            GameSettings.Player.PositionX = transform.position.x;
            GameSettings.Player.PositionY = transform.position.y;
            GameSettings.Player.PositionZ = transform.position.z;

            GameSettings.Player.RotationY = Quaternion.LookRotation(transform.forward).eulerAngles.y;

            GameSettings.Player.Health = (int) _health.m_CurrentHealth;
        }
    }
}
