#include <list>
#include <iostream>
#include <sstream>

#include "GameManager.h"
#include "LobbyManager.h"
#include "ClientManager.h"

GameManager* GameManager::_instance = 0;

GameManager::GameManager()
{
	// singleton

	_broadcastMessage = new BroadcastMessage;
}

GameManager::~GameManager()
{
	free((void*)_broadcastMessage);
}

Response* GameManager::ProceedRequest(Request* request, std::int32_t owner)
{
	_hasBroadcastMessage = false;
	auto response = new Response;
	response->result = false;

	auto client = ClientManager::GetInstance()->GetClient(owner);

	switch (request->GetType())
	{
		case RequestType::GetAll:
		{
			if (client == nullptr) RequestBreak(response->message, "Fatal error on getting game! [Client not found!]");

			int id = 0;

			for (auto const& game : _games) for (auto const& c : game->GetClients()) if (c->GetName().compare(client->GetName()) == 0) { id = game->GetId(); client = c; }

			if (id == 0) RequestBreak(response->message, "You are not part of game!");

			auto game = getGame(id);

			response->message = game->ToString();
			response->result = true;

			break;
		}
		case RequestType::Synchronize:
		{
			if (client == nullptr) RequestBreak(response->message, "Fatal error on synchronizing game! [Client not found!]"); // should never hapenned

			auto body = request->GetBody();

			if (body.length() < 1) RequestBreak(response->message, "Missing paremeters! Request body should be [<x-position> <y-position> <z-position> <y-rotation> <health>] as integers");

			int id = 0;

			for (auto const& game : _games) for (auto const& c : game->GetClients()) if (c->GetName().compare(client->GetName()) == 0) { id = game->GetId(); client = c; }

			if (id == 0) RequestBreak(response->message, "You are not part of game!");

			auto game = getGame(id);

			std::istringstream ss(body);
			std::string token;
			std::vector<int32_t> parameters;

			while (getline(ss, token, ' '))
			{
				if (token.length() < 1)
				{
					response->message = "Game sync parameters cannot be empty!";

					return response;
				}

				bool isNumber = (token.find_first_not_of("-0123456789") == std::string::npos);

				if (!isNumber)
				{
					response->message = "Game sync parameters must be integer!";

					return response;
				}

				int32_t pos = std::stoi(token);

				parameters.push_back(pos);
			}

			if (parameters.size() != 5) RequestBreak(response->message, "Missing paremeters! Request body should be [<x-position> <y-position> <z-position> <y-rotation> <health>] as integers");

			client->SetPosition({ parameters[0], parameters[1], parameters[2] });
			client->SetRotationY(parameters[3]);
			client->SetHealth(parameters[4]);

			size_t aliveClients = 0;
			for (auto const& c : game->GetClients()) if (c->GetHealth() > 0) aliveClients++;

			if (aliveClients > 1) // game still running
			{
				response->message = std::to_string(id) + ";" + client->GetName() + ";" + client->GetPosition().toString() + ";" + std::to_string(parameters[3]) + ";" + std::to_string(parameters[4]);
				response->result = true;

				std::stringstream os;

				os << "GS" << response->ToString();

				_broadcastMessage->body = os.str();
				_broadcastMessage->clients = game->GetClients();
				response->result = _hasBroadcastMessage = true;
			}
			else // game is over
			{
				_games.remove(game);

				delete game;
			}

			break;
		}
		case RequestType::Create:
		{
			if (client == nullptr) RequestBreak(response->message, "Fatal error on creating game! [Client not found!]");

			auto lobby = LobbyManager::GetInstance()->GetLobby(client->GetName());

			if (lobby == nullptr) RequestBreak(response->message, "Game can be created only from lobby! [Lobby not found!]");

			auto game = CreateGame(client, lobby);

			if (game == nullptr) RequestBreak(response->message, ("Create game from lobby " + lobby->GetName() + " can only owner!"));

			response->result = LobbyManager::GetInstance()->RemoveLobby(lobby);

			if (!response->result) RequestBreak(response->message, ("Removing lobby " + lobby->GetName() + " when game creating failed!"));

			response->message = game->ToString();

			std::stringstream os;

			os << "GC" << response->ToString();

			_broadcastMessage->body = os.str();
			_broadcastMessage->clients = ClientManager::GetInstance()->GetClients();
			response->result = _hasBroadcastMessage = true;

			break;
		}
		case RequestType::Fire:
		{
			if (client == nullptr) RequestBreak(response->message, "Fatal error on synchronizing game! [Client not found!]"); // should never hapenned

			auto body = request->GetBody();

			if (body.length() < 1) RequestBreak(response->message, "Missing paremeters! Request body should be [<x-velocity> <y-velocity> <z-velocity>] as integers");

			int id = 0;

			for (auto const& game : _games) for (auto const& c : game->GetClients()) if (c->GetName().compare(client->GetName()) == 0) { id = game->GetId(); client = c; }

			if (id == 0) RequestBreak(response->message, "You are not part of game!");

			auto game = getGame(id);

			std::istringstream ss(body);
			std::string token;
			std::vector<int32_t> parameters;

			while (getline(ss, token, ' '))
			{
				if (token.length() < 1)
				{
					response->message = "Game sync parameters cannot be empty!";

					return response;
				}

				bool isNumber = (token.find_first_not_of("-0123456789") == std::string::npos);

				if (!isNumber)
				{
					response->message = "Game sync parameters must be integer!";

					return response;
				}

				int32_t pos = std::stoi(token);

				parameters.push_back(pos);
			}

			if (parameters.size() != 3) RequestBreak(response->message, "Missing paremeters! Request body should be [<x-velocity> <y-velocity> <z-velocity>] as integers");

			response->message = (client->GetName()) + ";" + std::to_string(parameters[0]) + ";" + std::to_string(parameters[1]) + ";" + std::to_string(parameters[2]);
			response->result = true;

			std::stringstream os;

			os << "GF" << response->ToString();

			_broadcastMessage->body = os.str();
			_broadcastMessage->clients = game->GetClients();
			response->result = _hasBroadcastMessage = true;
			break;
		}
		case RequestType::Destroy:
		{
			if (client == nullptr) RequestBreak(response->message, "Fatal error on synchronizing game! [Client not found!]"); // should never hapenned

			int id = 0;

			for (auto const& game : _games) for (auto const& c : game->GetClients()) if (c->GetName().compare(client->GetName()) == 0) { id = game->GetId(); client = c; }

			if (id == 0) RequestBreak(response->message, "You are not part of game!");

			auto game = getGame(id);

			response->result = game->RemoveClient(client);

			if (!response->result) RequestBreak(response->message, "Removing client from game failed!");

			response->message = client->GetName();

			std::stringstream os;

			os << "GD" << response->ToString();

			_broadcastMessage->body = os.str();
			_broadcastMessage->clients = game->GetClients();
			response->result = _hasBroadcastMessage = true;
			
			if (game->GetClients().size() < 1) DestroyGame(game);

			break;
		}
		default: response->message = "Request type not allowed in this context!";
	}

	return response;
}

Game* GameManager::getGame(int id)
{
	for (auto Game : _games) if (Game->GetId() == id) return Game;

	return nullptr;
}

GameManager *GameManager::GetInstance()
{
	if (_instance == 0)
		_instance = new GameManager();

	return _instance;
}

Game * GameManager::CreateGame(Client * owner, Lobby * lobby)
{
	if (lobby->GetOwner() != owner) return nullptr;

	auto game = new Game(lobby);

	_games.push_back(game);

	return game;
}

bool GameManager::DestroyGame(Game * game)
{
	bool found = false;

	for (auto const& g : _games) if (game->GetId() == g->GetId()) found = true;

	if (!found) return false;

	_games.remove(game);

	delete game;

	return true;
}

Game * GameManager::GetGame(std::string clientName)
{
	for (auto const& game : _games) for (auto const& client : game->GetClients()) if (client->GetName().compare(clientName) == 0) return game;

	return nullptr;
}

std::list<Game*> GameManager::GetGames()
{
	return _games;
};

bool GameManager::HasBroadcastMessage()
{
	return _hasBroadcastMessage;
}

BroadcastMessage* GameManager::GetBroadcastMessage()
{
	return _broadcastMessage;
}