#include <sstream>
#include "ClientManager.h"

ClientManager* ClientManager::_instance = NULL;

ClientManager::ClientManager()
{
	_broadcastMessage = new BroadcastMessage;
}

ClientManager * ClientManager::GetInstance()
{
	if (_instance == NULL) _instance = new ClientManager();

	return _instance;
}

Response* ClientManager::ProceedRequest(Request* request, std::int32_t owner)
{
	auto response = new Response;
	response->result = false;
	response->message = "";

	_hasBroadcastMessage = false;

	switch (request->GetType())
	{
		case RequestType::GetAll:
		{
			for (auto const& client : _clients) if(client.second->GetId() != owner) response->message += client.second->ToString() + ";";

			response->result = ((response->message).length() > 0);
			
			if (response->result)
				(response->message).pop_back();
			else
				response->message = "No other clients exist!";

			break;
		}
		case RequestType::Create:
		{
			auto name = request->GetBody();

			if (name.length() <= 0) RequestBreak(response->message, "Client name cannot be empty!");
			if (name.length() >= 14) RequestBreak(response->message, "Client name cannot be longer then 14 character!");

			// get client by active connection socket
			auto existClient = GetClient(owner);

			// client already connected
			if (existClient != nullptr) RequestBreak(response->message, "Client already connected as " + existClient->GetName() + "!");

			// get client by uid (name)
			existClient = GetClient(name);

			// clients already exist 
			if (existClient != nullptr)
			{
				// somebody is connected as this client
				if (existClient->GetId() > 0) RequestBreak(response->message, "Client with this name already connected!");

				// otherwise login
				existClient->SetConnection(owner);
			}
			// otherwise create new client
			else
			{
				existClient = new Client(owner, name);

				_clients[name] = existClient;
			}

			response->result = true;
			response->message = existClient->ToString();

			std::ostringstream os;

			os << "CS" << response->ToString();

			_broadcastMessage->body = os.str();
			_broadcastMessage->clients = GetClients();
			_hasBroadcastMessage = true;

			break;
		}
		default: response->message = "Request type not allowed in this context!";
	}

	return response;
}

bool ClientManager::HasBroadcastMessage()
{
	return _hasBroadcastMessage;
}

BroadcastMessage* ClientManager::GetBroadcastMessage()
{
	return _broadcastMessage;
}

std::list<Client*> ClientManager::GetClients()
{
	std::list<Client*> result;

	for (auto const& client : _clients) result.push_back(client.second);

	return result;
}

Client* ClientManager::GetClient(std::string name)
{
	Client* result = _clients[name];

	return result;
}

Client * ClientManager::GetClient(int32_t id)
{
	for (auto const& client : _clients) if (client.second->GetId() == id) return client.second;

	return nullptr;
}

ClientManager::~ClientManager()
{
	//TODO FREE MEMORY
	free((void*)_broadcastMessage);
}
