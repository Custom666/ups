#ifndef Request_h
#define Request_h

#include <string>
#include <map>
#include <vector>

enum class EntityType
{
	Unknown,
	ClientType,
	LobbyType,
	GameType
};

enum class RequestType
{
	Unknown,
	GetById,
	GetAll,
	SetName,
	Create,
	Join,
	Leave,
	Start,
	Destroy,
	Fire,
	Synchronize
};

static std::map<char, RequestType> GetRequestType =
{
   {'I', RequestType::GetById},
   {'A', RequestType::GetAll},
   {'N', RequestType::SetName},
   {'C', RequestType::Create},
   {'J', RequestType::Join},
   {'L', RequestType::Leave},
   {'R', RequestType::Start},
   {'D', RequestType::Destroy},
   {'F', RequestType::Fire},
   {'S', RequestType::Synchronize}
};

static std::map<char, EntityType> GetEntityType =
{
   {'C', EntityType::ClientType},
   {'L', EntityType::LobbyType},
   {'G', EntityType::GameType},
};

char GetEntityTypeChar(EntityType type);
char GetRequestTypeChar(RequestType type);

class Request
{

public:

	Request(std::string* request);
	~Request();

	EntityType GetEntity();
	RequestType GetType();
	std::string GetBody();

private:

	EntityType _entityType;
	RequestType _requestType;
	std::string _body;
};

#endif

