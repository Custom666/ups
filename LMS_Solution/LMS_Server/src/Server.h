#ifndef Server_h
#define Server_h

#include <list>
#include "Client.h"
#include "Request.h"
#include "BroadcastMessage.h"

class Server
{
public:
	bool Initialize(std::string ip, size_t port);
	void HandleConnection();
	int32_t GetSocket();

	static const int32_t MAX_CONNECTIONS = 32;
	static const int32_t MAX_REQUEST_LENGTH = 32;

private:
	int32_t _socket;

	static bool addConnection(int32_t newConnection);
	static bool removeConnection(int32_t connection);

	static void connectionHandler(int32_t clientId);
	static Request* handleRequest(int32_t clientId);
	static void disconnectClient(int32_t clientId);
	
	static void proceedRequest(Request* request, int32_t clientId);
	static void broadcast(BroadcastMessage* message, int32_t clientId);
};

#endif