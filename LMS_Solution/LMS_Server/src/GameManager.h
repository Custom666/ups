#ifndef GameManager_h
#define GameManager_h

#include <list>
#include "Client.h"
#include "ManagerBase.h"
#include "Game.h"

class GameManager : public ManagerBase
{
    public:
        static GameManager* GetInstance();
        Game* CreateGame(Client* owner, Lobby* lobby);
		
		Response* ProceedRequest(Request* request, std::int32_t owner);
		bool HasBroadcastMessage();
		BroadcastMessage* GetBroadcastMessage();

		bool DestroyGame(Game* game);
		Game* GetGame(std::string clientName);
		std::list<Game*> GetGames();

    private:
        GameManager();
		~GameManager();

		Game* getGame(int id);

		BroadcastMessage* _broadcastMessage;
		bool _hasBroadcastMessage;
        std::list<Game*> _games;
        static GameManager* _instance;
};

#endif