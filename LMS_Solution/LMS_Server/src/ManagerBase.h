#ifndef ManagerBase_h
#define ManagerBase_h

#include "Request.h"
#include "BroadcastMessage.h"
#include <sstream>
#include <iomanip>

#define RequestBreak(request, msg) { request = (msg); break; }

struct Response
{
	bool result;

	std::string message;

	std::string ToString() 
	{
		std::stringstream ss;

		ss << std::setw(3) << std::setfill('0') << message.length();
		
		return (result ? "K" : "N") + ss.str() + message;
	}
};

class ManagerBase
{

public:
	
	virtual Response* ProceedRequest(Request* request, std::int32_t owner) = 0;

	virtual bool HasBroadcastMessage() = 0;
	virtual BroadcastMessage* GetBroadcastMessage() = 0;
};

#endif