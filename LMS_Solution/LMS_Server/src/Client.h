#ifndef Client_h
#define Client_h

#include <string>

struct vector_3
{
	float x;
	float y;
	float z;

	std::string toString() const { return std::to_string(x) + ";" + std::to_string(y) + ";" + std::to_string(z); }
};

class Client
{
public:
	Client(int32_t id, std::string name);
	std::string ToString();

	int32_t GetId();
	std::string GetName();
	int32_t GetHealth();
	vector_3 GetPosition();
	int32_t GetRotationY();

	void SetConnection(int32_t connected);
	bool IsConnected();
	bool SetName(std::string name);
	bool SetPosition(vector_3 position);
	bool SetRotationY(int32_t rotation);
	bool SetHealth(int32_t health);

private:
	int32_t _id;
	std::string _name;
	int32_t _health;
	vector_3 _position;
	int32_t _rotationY;
};

#endif