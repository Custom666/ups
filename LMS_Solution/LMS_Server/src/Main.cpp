#include <sys/types.h>
//#include <sys/un.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include "Server.h"
#include "ClientManager.h"

#include "spdlog/spdlog.h"
#include "spdlog/sinks/file_sinks.h"

int main(int argc, char* argv[])
{
	std::string ip = "";
	size_t port = 0;
	std::shared_ptr<spdlog::logger> logger;

	if (argc < 2 || argc > 4)
	{
		std::cout << ("Server: initializing error! Wrong arguments count!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.") << std::endl;
		return EXIT_FAILURE;
	}

	if (argc == 2)
	{
		auto const& argument = std::string(argv[1]);

		if (argument.compare("f") == 0)
		{
			logger = spdlog::rotating_logger_mt("all", "all.txt", 1048576 * 20, 3);
			logger->flush_on(spdlog::level::info);
		}
		else if (argument.compare("c") == 0)
		{
			logger = spdlog::stdout_logger_mt("all");
		}
		else
		{
			std::cout << ("Server: initializing error! Wrong arguments!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.") << std::endl;
			return EXIT_FAILURE;
		}
	}

	if (argc == 3)
	{
		auto const& loggerArgument = std::string(argv[1]);
		ip = std::string(argv[2]);

		if (loggerArgument.compare("f") == 0)
		{
			logger = spdlog::rotating_logger_mt("all", "all.txt", 1048576 * 20, 3);
			logger->flush_on(spdlog::level::info);
		}
		else if (loggerArgument.compare("c") == 0)
		{
			logger = spdlog::stdout_logger_mt("all");
		}
		else
		{
			logger->error("Server: initializing error! Wrong arguments!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.");
			return EXIT_FAILURE;
		}

		if (ip.empty())
		{
			logger->error("Server: initializing error! Wrong arguments!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.");
			return EXIT_FAILURE;
		}
	}

	if (argc == 4)
	{
		auto const& loggerArgument = std::string(argv[1]);
		ip = std::string(argv[2]);
		auto const& portArgument = std::string(argv[3]);

		if (loggerArgument.compare("f") == 0)
		{
			logger = spdlog::rotating_logger_mt("all", "all.txt", 1048576 * 20, 3);
			logger->flush_on(spdlog::level::info);
		}
		else if (loggerArgument.compare("c") == 0)
		{
			logger = spdlog::stdout_logger_mt("all");
		}
		else
		{
			logger->error("Server: initializing error! Wrong arguments!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.");
			return EXIT_FAILURE;
		}

		if (ip.empty())
		{
			logger->error("Server: initializing error! Wrong arguments!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.");
			return EXIT_FAILURE;
		}

		if (portArgument.empty())
		{
			logger->error("Server: initializing error! Wrong arguments!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.");
			return EXIT_FAILURE;
		}

		bool isNumber = (portArgument.find_first_not_of("0123456789") == std::string::npos);

		if (!isNumber)
		{
			logger->error("Server: initializing error! Wrong arguments!\nFirst argument is [c] for logging into console or [f] for logging into file.\nSecond argument is optional [ip-address]. Default is 127.0.0.1.\nThird argument is also optional [port]. Default is 10000.");
			return EXIT_FAILURE;
		}

		port = std::stoi(portArgument);
	}

	std::list<Client*> clients;
	class Server *server = new Server();
	
	bool initialized = server->Initialize(ip, port);

	if (initialized)
		logger->info("Server: initialized successfully");
	else
	{
		logger->error("Server: initializing error %d", initialized);
		return EXIT_FAILURE;
	}

	while (true) server->HandleConnection();

	return EXIT_SUCCESS;
}
