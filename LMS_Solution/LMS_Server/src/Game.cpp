#include <list>
#include "Game.h"

Game::Game(Lobby * lobby)
{
	_id = lobby->GetId();
	_capacity = lobby->GetCapacity();
	_clients = lobby->GetClients();
	_clients.push_back(lobby->GetOwner());

	float x = 0, y = 10, z = 0;

	for (auto client : _clients)
	{
		vector_3 position;
		position.x = x;
		position.y = y;
		position.z = z;

		client->SetPosition(position);

		z += 10;
	}
}

Game::~Game()
{}

std::string Game::ToString()
{
	auto result = std::to_string(_id) +
		";" + std::to_string(_capacity) +
		";" + std::to_string(_clients.size());

	for (auto c : _clients) result += ";" + c->GetName() + ";" + c->GetPosition().toString() + ";" + std::to_string(c->GetHealth());

	return result;
}

std::int32_t Game::GetId()
{
	return _id;
}

bool Game::RemoveClient(Client * client)
{
	bool canRemove = false;
	for (auto const& c : _clients) if (c->GetName().compare(client->GetName()) == 0) canRemove = true;

	if (!canRemove) return false;

	_clients.remove(client);

	return true;
}

std::list<Client*> Game::GetClients()
{
	return _clients;
}

std::int32_t Game::GetCapacity()
{
	return _capacity;
}