#include "Request.h"
#include <string>
#include <algorithm>

Request::Request(std::string* request)
{
	auto req = *request;

	_entityType = EntityType::Unknown;
	_requestType = RequestType::Unknown;

	if (req.length() > 1 && GetEntityType.find(req[0]) != GetEntityType.end())
		_entityType = GetEntityType[req[0]]; // entity type

	if (req.length() > 2 && GetRequestType.find(req[1]) != GetRequestType.end())
		_requestType = GetRequestType[req[1]]; // request type

	if (req.length() > 3)
		_body = _entityType == EntityType::Unknown ? req : req.substr(2); // request body
	
	if (_body.length() > 0) // remove all non-alpha and non-digit characters from body (except " " and "-")
		_body.erase(std::remove_if(_body.begin(), _body.end(), [](char c) { return !isalpha(c) && !isdigit(c) && !(c == ' ') && !(c == '-'); }), _body.end());
}

char GetEntityTypeChar(EntityType type)
{
	for (auto &i : GetEntityType) if (i.second == type) return i.first;

	return 'U';
}

char GetRequestTypeChar(RequestType type)
{
	for (auto &i : GetRequestType) if (i.second == type) return i.first;

	return 'U';
}

Request::~Request()
{
}

EntityType Request::GetEntity()
{
	return _entityType;
}

RequestType Request::GetType()
{
	return _requestType;
}

std::string Request::GetBody()
{
	return _body;
}
