#ifndef ClientManager_h
#define ClientManager_h

#include <stdlib.h>
#include <list>
#include "ManagerBase.h"
#include "Client.h"

class ClientManager : public ManagerBase
{

public:
	static ClientManager* GetInstance();

	Response* ProceedRequest(Request* request, std::int32_t owner);
	bool HasBroadcastMessage();
	BroadcastMessage* GetBroadcastMessage();

	Client* GetClient(std::string name);
	Client* GetClient(int32_t id);

	std::list<Client *> GetClients();
	

private:
	ClientManager();
	~ClientManager();

	BroadcastMessage* _broadcastMessage;
	bool _hasBroadcastMessage;
	static ClientManager* _instance;
	std::map<std::string, Client*> _clients;
};

#endif

