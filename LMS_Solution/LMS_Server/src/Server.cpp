#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <string>
#include <iostream>
#include <sstream>
#include <arpa/inet.h> 
#include <thread>
#include <mutex>

#include "Server.h"
#include "LobbyManager.h"
#include "ClientManager.h"
#include "GameManager.h"

#include "spdlog/spdlog.h"
#include "spdlog/sinks/file_sinks.h"

std::mutex incomingMessageMutex;
std::mutex disconnectingMutex;
std::mutex authenticationMutex;

int32_t _connections[Server::MAX_CONNECTIONS];

int32_t _waiters[Server::MAX_CONNECTIONS];

#define PrintMessage(value, message, error) if((value) < 0) { std::cerr << error << std::endl; return false; } else std::cout << message << std::endl;

#define Zero(arr, len) for(auto i = 0; i < len; i++) arr[i] = '\0';

bool Server::Initialize(std::string ip, size_t port)
{
	sockaddr_in address;
	int32_t returnValue;

	Zero(_connections, MAX_CONNECTIONS);

	_socket = socket(AF_INET, SOCK_STREAM, 0);

	PrintMessage(_socket, "socket OK", "socket ERR");

	memset(&address, 0, sizeof(struct sockaddr_in));

	address.sin_family = AF_INET;
	address.sin_port = htons(port == 0 ? 10000 : port);
	address.sin_addr.s_addr = ip.empty() ? INADDR_ANY : inet_addr(ip.c_str()); //INADDR_ANY;

	// nastavime parametr SO_REUSEADDR - "znovupouzije" puvodni socket, co jeste muze hnit v systemu bez predchoziho close
	int param = 1;
	returnValue = setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&param, sizeof(int));

	if (returnValue < 0) std::cout << "setsockopt ERR" << std::endl; else std::cout << "setsockopt OK" << std::endl;

	returnValue = bind(_socket, (struct sockaddr *)&address, sizeof(struct sockaddr_in));

	PrintMessage(returnValue, "bind OK", "bind ERR");

	returnValue = listen(_socket, 5);

	PrintMessage(returnValue, "listen OK", "listen ERR");

	return true;
}

void Server::HandleConnection()
{
	auto logger = spdlog::get("all");

	socklen_t addrlen = 0;
	struct sockaddr_in address;
	memset(&address, 0, sizeof(struct sockaddr_in));

	auto newConnection = accept(_socket, (struct sockaddr *)&address, &addrlen);

	// add new connection to array of connections
	auto result = addConnection(newConnection);

	if (!result)
	{
		logger->error("Accepting new connection failed! [{}]", std::strerror(errno));
		return;
	}

	struct timeval timeout;
    timeout.tv_sec = 21;
	setsockopt(newConnection, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

	logger->info("Accepted new connection[{}] {}:{}", newConnection, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

	std::thread t(connectionHandler, newConnection);

	t.detach();
}

void Server::connectionHandler(int32_t clientId)
{
	auto logger = spdlog::get("all");

	ClientManager* clientManager = ClientManager::GetInstance();
	Request* request = nullptr;

	logger->info("[{}] Starting handling request...", clientId);

	do
	{
		request = handleRequest(clientId); // store incoming message into _messages

		if (request == nullptr) break;

		std::lock_guard<std::mutex> lock(incomingMessageMutex);

		logger->info("[{}] request [EntityType[{}] RequestType[{}] Body[{}]", clientId, GetEntityTypeChar(request->GetEntity()), GetRequestTypeChar(request->GetType()), request->GetBody());

		auto client = clientManager->GetClient(clientId);

		if (client == nullptr && (request->GetEntity() != EntityType::ClientType || request->GetType() != RequestType::Create)) // invalid authentication request
		{
			auto response = new Response;
			response->result = false;
			response->message = "NOT AUTHENTICATION REQUEST!";

			std::ostringstream os;

			os << "UU" << response->ToString();

			auto const& msg = os.str();

			size_t sendResult = send(clientId, msg.c_str(), msg.length(), 0);

			if (sendResult > 0 && (size_t)sendResult == msg.length()) // Sended response
			{
				logger->info("[{}] sending response [{}]", clientId, msg);
			}
			else // Error
			{
				logger->error("[{}] sending response [{}] FAILED! Expected response length: {}, actual response length: {}.", clientId, msg, msg.length(), sendResult);
			}
		}
		else proceedRequest(request, clientId); // valid request

		delete request;

	} while (request != nullptr);

	logger->info("[{}] Finishing handling request...", clientId);
}

Request* Server::handleRequest(int32_t clientId)
{
	auto logger = spdlog::get("all");

	int32_t received = 0;
	Request* request = nullptr;

	char recvMessage[MAX_REQUEST_LENGTH + 1];
	Zero(recvMessage, MAX_REQUEST_LENGTH);

	logger->info("[{}] Waiting for incoming message...", clientId);

	received = recv(clientId, recvMessage, MAX_REQUEST_LENGTH, MSG_WAITALL);

	recvMessage[MAX_REQUEST_LENGTH] = '\0';

	if (received > 0) // incoming request
	{
		std::string message(recvMessage);

		logger->info("[{}] Parsing request [{}]", clientId, message);

		request = new Request(&message);
	}
	else if (received < 0)
	{
		disconnectClient(clientId);
	}

	return request;
}

void Server::proceedRequest(Request* request, int32_t clientId)
{
	auto logger = spdlog::get("all");

	ManagerBase* manager = nullptr;
	Response* response = nullptr;

	switch (request->GetEntity())
	{
		case EntityType::Unknown: // invalid request

			response = new Response;
			response->message = "WRONG REQUEST! Entity type was not recognize!";
			response->result = false;

			break;
		case EntityType::ClientType:
		{
			manager = ClientManager::GetInstance();
			break;
		}
		case EntityType::LobbyType:
		{
			manager = LobbyManager::GetInstance();
			break;
		}
		case EntityType::GameType:
		{
			manager = GameManager::GetInstance();
			break;
		}
	}

	if (manager) response = manager->ProceedRequest(request, clientId); // Request entity type is valid => proceed 

	std::ostringstream os;

	os << GetEntityTypeChar(request->GetEntity()) << GetRequestTypeChar(request->GetType()) << response->ToString();

	auto const& responseMessage = os.str();

	auto sendResult = send(clientId, responseMessage.c_str(), responseMessage.length(), 0);

	if (sendResult > 0 && (size_t)sendResult == responseMessage.length()) // Sended response
		logger->info("[{}] sending response [{}]", clientId, responseMessage);
	else // Error
		logger->error("[{}] sending response [{}] FAILED! Expected response length: {}, actual response length: {}.", clientId, responseMessage, responseMessage.length(), sendResult);

	if (manager && manager->HasBroadcastMessage()) broadcast(manager->GetBroadcastMessage(), clientId); // broadcast

	delete response;
}

void Server::disconnectClient(int32_t clientId)
{
	auto logger = spdlog::get("all");

	auto clientManager = ClientManager::GetInstance();
	auto lobbyManager = LobbyManager::GetInstance();
	auto gameManager = GameManager::GetInstance();

	auto response = new Response;
	std::string msg;

	auto client = clientManager->GetClient(clientId);

	if (client != nullptr) // exist client disconnecting
	{
		std::lock_guard<std::mutex> lock(disconnectingMutex);

		auto lobby = lobbyManager->GetLobby(client->GetName());

		if (lobby != nullptr) // send request to remove lobby if exist
		{
			response->result = lobbyManager->RemoveLobby(lobby);

			if (response->result)
			{
				response->message = std::to_string(lobby->GetId());
				
				std::ostringstream os; os << "LD" << response->ToString();
		
				msg = os.str();

				for (auto const& client : clientManager->GetClients()) if (client->GetId() > 0) send(client->GetId(), msg.c_str(), msg.length(), 0);
			}
		}

		auto game = gameManager->GetGame(client->GetName());

		if (game != nullptr) // destroy game if there is one or zero connected clients to it
		{
			int32_t connectedClients = -1;

			for (auto const& client : game->GetClients()) if (client->IsConnected()) connectedClients++;

			if (connectedClients < 2) gameManager->DestroyGame(game);
		}

		client->SetConnection(0);

		response->result = true;
		response->message = std::to_string(clientId);
		
		std::ostringstream os; os << "CD" << response->ToString();
		msg = os.str();

		for (auto const& client : clientManager->GetClients()) if (client->GetId() > 0) send(client->GetId(), msg.c_str(), msg.length(), 0);
	}

	auto result = removeConnection(clientId);

	if (!result) { logger->error("[{}] removing connection FAILED!", clientId); return; }

	close(clientId);

	logger->info("[{}] is disconnected", clientId);
}

void Server::broadcast(BroadcastMessage* message, int32_t clientId)
{
	auto logger = spdlog::get("all");
	int32_t id;

	for (auto c : message->clients)
	{
		id = c->GetId();

		if (id == clientId || id < 1) continue;

		ssize_t sendResult = send(id, message->body.c_str(), message->body.length(), 0);

		if (sendResult > 0 && message->body.length() > 0)
			logger->info("Broadcast {}[{}] message [{}]", c->GetName(), c->GetId(), message->body);
		else
			logger->error("Broadcast {}[{}] message [{}] ERROR", c->GetName(), c->GetId(), message->body);
	}
}

int32_t Server::GetSocket()
{
	return _socket;
}

bool Server::addConnection(int32_t newConnection)
{
	if (newConnection < 0) return false;

	for (auto i = 0; i < MAX_CONNECTIONS; i++) if (_connections[i] <= 0) { _connections[i] = newConnection; return true; }

	return false;
}

bool Server::removeConnection(int32_t connection)
{
	if (connection < 0) return false;

	for (auto i = 0; i < MAX_CONNECTIONS; i++) if (_connections[i] == connection) { _connections[i] = 0; return true; }

	return false;
}
