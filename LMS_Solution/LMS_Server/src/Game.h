#ifndef Game_h
#define Game_h

#include <string>
#include <list>
#include "Lobby.h"

class Game
{

public:
	Game(Lobby* lobby);
	~Game();

	std::string ToString();
	std::int32_t GetId();
	std::int32_t GetCapacity();

	bool RemoveClient(Client* client);
	std::list<Client*> GetClients();

private:

	std::int32_t		_id;
	std::list<Client *> _clients;
	std::int32_t		_capacity;
};

#endif
