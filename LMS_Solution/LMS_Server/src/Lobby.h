#ifndef Lobby_h
#define Lobby_h

#include <string>
#include <list>
#include "Client.h"

class Lobby
{

public:
	Lobby(Client* owner, std::string name, int capacity);
	~Lobby();

	bool AddClient(Client* client);
	bool RemoveClient(Client* client);

	std::string ToString();

	std::int32_t GetId();
	std::string GetName();
	Client* GetOwner();
	std::int32_t GetCapacity();
	std::list<Client*> GetClients();

private:

	std::int32_t		_id;
	std::string			_name;
	Client*				_owner;
	std::list<Client *> _clients;
	std::int32_t				_capacity;
};

#endif
