#include <algorithm>
#include <list>
#include "Lobby.h"

Lobby::Lobby(Client * owner, std::string name, int capacity)
{
	static int32_t id = 1;

	_id = id++;
	_owner = owner;
	_name = name;
	_capacity = capacity;
}

Lobby::~Lobby()
{
	//_clients.clear();
}

bool Lobby::AddClient(Client * client)
{
	if (client->GetName() == _owner->GetName()) return false;

	bool existClient = (std::find(_clients.begin(), _clients.end(), client) != _clients.end());

	if (existClient) return false;

	_clients.push_back(client);

	return true;
}

bool Lobby::RemoveClient(Client * client)
{
	bool clientToRemoveFound = false;
	Client* c;
	std::list<Client*>::iterator iterator;

	for(iterator = _clients.begin(); iterator != _clients.end(); iterator++)
	{
		c = *iterator;

		if(c->GetId() == client->GetId())
		{
			clientToRemoveFound = true;
			break;
		}
	}

	if(!clientToRemoveFound) return false;

	_clients.erase(iterator);

	return true;
}

std::string Lobby::ToString()
{
	auto result = std::to_string(_id) +
		";" + _name +
		";" + _owner->GetName() +
		";" + std::to_string(_capacity) +
		";" + std::to_string(_clients.size());

	for(auto c : _clients) result += ";" + c->GetName();

	return result;
}

std::int32_t Lobby::GetId()
{
	return _id;
}

std::string Lobby::GetName()
{
	return _name;
}

Client * Lobby::GetOwner()
{
	return _owner;
}

std::int32_t Lobby::GetCapacity()
{
	return _capacity;
}

std::list<Client*> Lobby::GetClients()
{
	return _clients;
}
