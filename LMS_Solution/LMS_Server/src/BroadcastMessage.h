#ifndef BroadcastMessage_h
#define BroadcastMessage_h

#include <list>
#include "Client.h"

struct BroadcastMessage
{
	std::string body;
	std::list<Client*> clients;

};

#endif