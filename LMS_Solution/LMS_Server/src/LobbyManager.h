#ifndef LobbyManager_h
#define LobbyManager_h

#include <list>
#include "Client.h"
#include "ManagerBase.h"
#include "Lobby.h"

class LobbyManager : public ManagerBase
{
    public:
        static LobbyManager* GetInstance();
		
		Response* ProceedRequest(Request* request, std::int32_t owner);
		bool HasBroadcastMessage();
		BroadcastMessage* GetBroadcastMessage();

        Lobby* CreateLobby(Client* owner, std::string name);
		Lobby* GetLobby(int id);
		Lobby* GetLobby(std::string clientName);
		bool RemoveLobby(Lobby* lobby);

		std::list<Lobby*> GetLobbies();

		const std::size_t MAX_LOBBY_NAME_LENGTH = 13;

    private:
        LobbyManager();
		~LobbyManager();

		BroadcastMessage* _broadcastMessage;
		bool _hasBroadcastMessage;
        std::list<Lobby*> _lobbies;
        static LobbyManager* _instance;
};

#endif