#include <string>

#include "Client.h"

Client::Client(int32_t id, std::string name)
{
    _id = id;
    _name = name;
    _health = 100;
	_position = vector_3{ 0.0, 5.0, 0.0 };
	_rotationY = 0.0;
}

int32_t Client::GetId()
{
    return _id;
}

int32_t Client::GetHealth()
{
    return _health;
}

vector_3 Client::GetPosition()
{
	return _position;
}

int32_t Client::GetRotationY()
{
	return _rotationY;
}

std::string Client::GetName()
{
    return _name;
}

void Client::SetConnection(int32_t connected)
{
	_id = connected > 0 ? connected : 0;
}

bool Client::IsConnected()
{
	return _id > 0;
}

bool Client::SetName(std::string name)
{
	//TODO validation

	_name = name;

	return true;
}

bool Client::SetPosition(vector_3 position)
{
	_position = position;

	//TODO validation

	return true;
}

bool Client::SetRotationY(int32_t rotation)
{
	_rotationY = rotation;

	//TODO validation

	return true;
}

bool Client::SetHealth(int32_t health)
{
	_health = health > 0 ? _health : 0;
	//TODO validation
	return true;
}

std::string Client::ToString()
{
	return std::to_string(_id) + ";" + _name;
}