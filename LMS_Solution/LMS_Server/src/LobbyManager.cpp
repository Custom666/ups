#include <list>
#include <sstream>

#include "LobbyManager.h"
#include "GameManager.h"
#include "ClientManager.h"

LobbyManager* LobbyManager::_instance = 0;

LobbyManager::LobbyManager()
{
	// singleton

	_broadcastMessage = new BroadcastMessage;
}

LobbyManager::~LobbyManager()
{
	free((void*)_broadcastMessage);
}

Response* LobbyManager::ProceedRequest(Request* request, std::int32_t owner)
{
	auto response = new Response;
	response->result = false;
	response->message = "";

	_hasBroadcastMessage = false;

	auto client = ClientManager::GetInstance()->GetClient(owner);
	auto clients = ClientManager::GetInstance()->GetClients();

	switch (request->GetType())
	{
		case RequestType::GetAll:
		{
			for (auto const& lobby : _lobbies) response->message += lobby->ToString() + ";";

			response->result = ((response->message).length() > 0);

			if (response->result)
				(response->message).pop_back();
			else
				response->message = "No lobby exist!";

			break;
		}
		case RequestType::Create:
		{
			auto body = request->GetBody();

			if (body.length() < 2) RequestBreak(response->message, "Missing paremeters! Request body should be [<capacity><name>]");

			std::string capacity(1, body[0]);
			auto name = body.substr(1);

			bool isNumber = (capacity.find_first_not_of("123456789") == std::string::npos);

			if (!isNumber) RequestBreak(response->message, "Lobby maximum capacity has to be number between <1 ; 9> !");

			if (name.length() > MAX_LOBBY_NAME_LENGTH) RequestBreak(response->message, "Name cannot be longer then " + std::to_string(MAX_LOBBY_NAME_LENGTH) + "characters!");

			for (auto const& lobby : _lobbies)
			{
				if ((lobby->GetName()).compare(name) == 0)
				{
					response->message = "Lobby with this name already exist!";

					return response;
				}

				if ((lobby->GetOwner()->GetName()).compare(client->GetName()) == 0)
				{
					response->message = "You cannot own more then one lobby at time!";

					return response;
				}
			}

			auto newLobby = new Lobby(client, name, std::stoi(capacity));

			_lobbies.push_back(newLobby);

			response->message = newLobby->ToString();
			response->result = true;

			std::ostringstream os;

			os << "LS" << response->ToString();

			_broadcastMessage->body = os.str();
			_broadcastMessage->clients = clients;
			_hasBroadcastMessage = true;

			break;
		}
		case RequestType::Join:
		{
			auto body = request->GetBody();

			if (body.length() < 1) RequestBreak(response->message, "Missing required parameter: <id> !");

			bool isNumber = (body.find_first_not_of("0123456789") == std::string::npos);

			if (!isNumber) RequestBreak(response->message, "Lobby required parameter <id> has to be number!");

			auto id = std::stoi(body);

			auto lobby = GetLobby(id);

			if (lobby == nullptr) RequestBreak(response->message, "Lobby not found!");

			bool isGame = false;

			// potentional lobby to delete
			for (auto const& game : GameManager::GetInstance()->GetGames()) if (game->GetId() == lobby->GetId()) { isGame = true; break; }

			if (isGame)  RequestBreak(response->message, "Game was started from this lobby!");

			response->result = lobby->AddClient(client);

			if (!response->result) RequestBreak(response->message, "Client is already part of this lobby!");

			response->message = lobby->ToString();

			std::ostringstream os;

			os << "LS" << response->ToString();

			_broadcastMessage->body = os.str();
			_broadcastMessage->clients = clients;
			_hasBroadcastMessage = true;

			break;
		}
		case RequestType::Leave:
		{
			auto lobby = GetLobby(client->GetName());

			if (lobby == nullptr) RequestBreak(response->message, "Lobby not found!");

			auto id = lobby->GetId();

			if (client->GetName() == lobby->GetOwner()->GetName()) // client is owner => destroy lobby
			{
				_lobbies.remove(lobby);

				delete lobby;

				response->result = true;
				response->message = std::to_string(id);

				std::ostringstream os;

				os << "LD" << response->ToString();

				_broadcastMessage->body = os.str();
				_broadcastMessage->clients = clients;
				_hasBroadcastMessage = true;
			}
			else // otherwise leave
			{
				response->result = lobby->RemoveClient(client);

				if (!response->result) RequestBreak(response->message, "Client not found!"); // This should not ever happen

				std::ostringstream os;

				response->message = lobby->ToString();

				os << "LS" << response->ToString();

				_broadcastMessage->body = os.str();
				_broadcastMessage->clients = clients;
				_hasBroadcastMessage = true;
			}

			response->message = std::to_string(id);

			break;
		}
		default: response->message = "Request type not allowed in this context!";
	}

	return response;
}

LobbyManager *LobbyManager::GetInstance()
{
	if (_instance == 0)
		_instance = new LobbyManager();

	return _instance;
}

Lobby* LobbyManager::GetLobby(int id)
{
	for (auto lobby : _lobbies) if (lobby->GetId() == id) return lobby;

	return nullptr;
}

Lobby* LobbyManager::GetLobby(std::string clientName)
{
	for (auto lobby : _lobbies)
	{
		if (lobby->GetOwner()->GetName() == clientName) return lobby;

		for (auto client : lobby->GetClients()) if (client->GetName() == clientName) return lobby;
	}

	return nullptr;
}

bool LobbyManager::RemoveLobby(Lobby* lobby)
{
	bool lobbyToDelete = false;

	for (auto const& l : _lobbies) if (l->GetId() == lobby->GetId()) lobbyToDelete = true;

	if (!lobbyToDelete) return false;

	_lobbies.remove(lobby);

	delete lobby;

	return true;
}

std::list<Lobby *> LobbyManager::GetLobbies()
{
	return _lobbies;
};

bool LobbyManager::HasBroadcastMessage()
{
	return _hasBroadcastMessage;
}

BroadcastMessage* LobbyManager::GetBroadcastMessage()
{
	return _broadcastMessage;
}