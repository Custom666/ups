﻿using LMS_Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LMS_Core.Models
{
    public class Lobby : POCO, IEquatable<Lobby>
    {
        public ObservableCollection<NetworkClient> Clients { get; private set; }

        public readonly string Name;
        public readonly NetworkClient Owner;
        public readonly int Capacity;

        public Lobby() : base()
        {
            Name = "Unknown";
            Owner = null;
            Capacity = 5;

            Clients = new ObservableCollection<NetworkClient>();
        }

        public Lobby(string name, int capacity, NetworkClient owner) : base()
        {
            Name = name;
            Capacity = capacity;
            Owner = owner;

            Clients = new ObservableCollection<NetworkClient>();
        }

        public Lobby(int id, string name, int capacity, NetworkClient owner) : this(name, capacity, owner)
        {
            Id = id;
        }

        public override string ToString()
        {
            return $"{ Name };{ Owner.Name };{ Capacity }";
        }

        public override bool Equals(object obj)
        {
            return obj is Lobby lobby && this.Equals(lobby);
        }

        public bool Equals(Lobby other)
        {
            return other != null &&
                   Name == other.Name &&
                   EqualityComparer<NetworkClient>.Default.Equals(Owner, other.Owner) &&
                   Capacity == other.Capacity;
        }

        public override int GetHashCode()
        {
            var hashCode = -1024705629;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<NetworkClient>.Default.GetHashCode(Owner);
            hashCode = hashCode * -1521134295 + Capacity.GetHashCode();
            return hashCode;
        }
    }
}