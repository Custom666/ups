﻿using LMS_Core.Enums;
using LMS_Core.Infrastructure;

namespace LMS_Core.Models
{
    public class Response : IResponse
    {
        public EntityType EntityType { get; set; }

        public RequestType RequestType { get; set; }

        public ResultType ResultType { get; set; }

        public string Body { get; set; }
        
        public Response()
        {
        }
    }
}