﻿using LMS_Core.Enums;
using LMS_Core.Infrastructure;

namespace LMS_Core.Models
{
    public class Error : POCO
    {
        public string Message { get; set; }

        public EntityType EntityType { get; set; }

        public RequestType RequestType{ get; set; }

        public Error() : base()
        {
            Message = string.Empty;
            EntityType = EntityType.Unknown;
            RequestType = RequestType.Unknown;
        }

        public override string ToString()
        {
            return $"[{ Message }]";
        }
    }
}