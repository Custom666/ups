﻿using LMS_Core.Infrastructure;
using System.Numerics;

namespace LMS_Core.Models
{
    public class NetworkClient : POCO
    {
        public readonly string Name;

        public bool IsConnected => Id > 0;

        public int Health { get; set; } = 100;

        public float PositionX { get; set; } = 0f;
        public float PositionY { get; set; } = 0f;
        public float PositionZ { get; set; } = 0f;

        public float RotationY { get; set; } = 0f;

        public NetworkClient()
        {
        }

        public NetworkClient(int id) : base(id)
        {
        }

        public NetworkClient(int id, string name) : this(id)
        {
            Name = name;
        }
        
        public override string ToString()
        {
            return $"{ Id };{ Name }";
        }
    }
}
