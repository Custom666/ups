﻿using LMS_Core.Infrastructure;
using System.Collections.ObjectModel;

namespace LMS_Core.Models
{
    public class Game : POCO
    {
        public readonly ObservableCollection<NetworkClient> Clients;
        public readonly int Capacity;

        public Game() : base()
        {
            Capacity = 5;
            Clients = new ObservableCollection<NetworkClient>();
        }

        public Game(int id, int capacity) : this()
        {
            Id = id;
            Capacity = capacity;
        }

        public override string ToString()
        {
            return $"{ Id };";
        }
    }
}