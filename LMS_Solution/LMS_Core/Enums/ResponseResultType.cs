﻿namespace LMS_Core.Enums
{
    public enum ResultType
    {
        Unknown, Ok, Error
    }

    public static class ResultTypeExtension
    {
        public static string ToMessageFormat(this ResultType type)
        {
            switch (type)
            {
                case ResultType.Ok: return "K";
                case ResultType.Error: return "N";
                default: return "U";
            }
        }

        public static bool TryParse(char value, out ResultType type)
        {
            switch (value)
            {
                case 'K': type = ResultType.Ok; return true;
                case 'N': type = ResultType.Error; return true;
                default: type = ResultType.Unknown; return false;
            }
        }
    }
}