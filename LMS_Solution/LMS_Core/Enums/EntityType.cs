﻿using System;

namespace LMS_Core.Enums
{
    public enum EntityType
    {
        Unknown, Client, Lobby, Game
    }

    public static class EntityTypeExtension
    {
        public static string ToMessageFormat(this EntityType type)
        {
            switch (type)
            {
                case EntityType.Client: return "C";
                case EntityType.Lobby: return "L";
                case EntityType.Game: return "G";
                default: return "U";
            }
        }

        public static bool TryParse(char value, out EntityType type)
        {
            switch (value)
            {
                case 'C': type = EntityType.Client; return true;
                case 'L': type = EntityType.Lobby; return true;
                case 'G': type = EntityType.Game; return true;
                case 'U': type = EntityType.Unknown; return true;
                default: type = EntityType.Unknown;  return false;
            }
        }
    }
}