﻿namespace LMS_Core.Enums
{
    public enum ServiceStatus
    {
        Disconnected,
        Connecting,
        Connected,
        Authenticating,
        Authenticated
    }
}
