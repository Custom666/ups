﻿namespace LMS_Core.Enums
{
    public enum RequestType
    {
        Unknown, Synchronize, GetAll, GetById, Create, Join, Leave, Fire, Destroy
    }

    public static class RequestTypeExtension
    {
        public static string ToMessageFormat(this RequestType type)
        {
            switch (type)
            {
                case RequestType.Synchronize: return "S";
                case RequestType.GetAll: return "A";
                case RequestType.GetById: return "I";
                case RequestType.Create: return "C";
                case RequestType.Join: return "J";
                case RequestType.Leave: return "L";
                case RequestType.Fire: return "F";
                case RequestType.Destroy: return "D";

                default: return "U";
            }
        }

        public static bool TryParse(char value, out RequestType type)
        {
            switch (value)
            {
                case 'S': type = RequestType.Synchronize; return true;
                case 'A': type = RequestType.GetAll; return true;
                case 'I': type = RequestType.GetById; return true;
                case 'C': type = RequestType.Create; return true;
                case 'J': type = RequestType.Join; return true;
                case 'L': type = RequestType.Leave; return true;
                case 'D': type = RequestType.Destroy; return true;
                case 'F': type = RequestType.Fire; return true;
                case 'U': type = RequestType.Unknown; return true;

                default: type = RequestType.Unknown; return false;
            }
        }
    }
}
