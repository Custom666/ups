﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS_Core.Extension
{
    public static class EncodingExtension
    {
        public static string ReadString(this Encoding encoding, byte[] cString)
        {
            var nullIndex = Array.IndexOf(cString, (byte) '\0'); 

            nullIndex = (nullIndex == -1) ? cString.Length : nullIndex;

            return encoding.GetString(cString, 0, nullIndex);
        }
    }
}
