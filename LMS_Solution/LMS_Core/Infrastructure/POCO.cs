﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS_Core.Infrastructure
{
    public abstract class POCO : IPOCO, IEquatable<POCO>
    {
        public int Id { get; set; }

        public POCO()
        {
            Id = -1;
        }

        public POCO(int id)
        {
            Id = id;
        }

        public bool Equals(POCO other)
        {
            return Id == other.Id;
        }
    }
}
