﻿using LMS_Core.Enums;

namespace LMS_Core.Infrastructure
{
    public interface IResponse
    {
        ResultType ResultType { get; }

        RequestType RequestType { get; }

        EntityType EntityType { get; }

        string Body { get; }
    }
}