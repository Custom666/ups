﻿using System;
using System.Threading.Tasks;

namespace LMS_Core.Infrastructure
{
    public interface IService : IDisposable
    {
        Enums.ServiceStatus ServiceStatus { get; }

        Task<bool> ConnectAsync(string connectionString);

        void Send(string request);

        void Authenticate();

        //void BeginReceive();

        void Disconnect();
    }
}
