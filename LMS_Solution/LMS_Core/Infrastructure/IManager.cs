﻿using LMS_Core.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace LMS_Core.Infrastructure
{
    public interface IManager<T> where T: IPOCO, new()
    {
        ObservableCollection<T> Items { get; }

        //Task<T> GetAsync(int id);

        //Task<IList<T>> GetAllAsync();

        bool Set(T poco, RequestType type);
    }
}
