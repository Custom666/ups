﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using LMS_Core.Models;

namespace LMS_Core
{
    public class GameSettings
    {
        public static string PlayerName { get; set; }

        public static IPAddress IpAddress{ get; set; }

        public static int Port{ get; set; }

        public static NetworkClient Player { get; internal set; }
    }
}
