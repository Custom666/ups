﻿using LMS_Core.Enums;
using LMS_Core.Extension;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS_Core.Services
{
    public class NetworkService : IService
    {
        public const int MAX_REQUEST_LENGTH = 32;
        public const int MAX_RESPONSE_LENGTH = 255;
        public const int CHECK_STATUS_INTERVAL = 15000;

        public delegate void OnRecieve(IResponse response);
        public static event OnRecieve OnReceiveEvent;

        public delegate void OnServiceStatusChanged();
        public static event OnServiceStatusChanged OnServiceStatusChangedEvent;

        public ServiceStatus ServiceStatus
        {
            get => _serviceStatus;

            private set
            {
                _serviceStatus = value;

                if (_serviceStatus == ServiceStatus.Disconnected && GameSettings.Player != null) GameSettings.Player.Id = 0;

                OnServiceStatusChangedEvent?.Invoke();
            }
        }

        private static Mutex _sendingMutex = new Mutex();

        private Timer _checkStatusTimer;

        private Socket _serverSocket;
        private IPEndPoint _endPoint;
        private ServiceStatus _serviceStatus;

        public NetworkService()
        {
            ServiceStatus = ServiceStatus.Disconnected;
            _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        private void checkStatusCallback(object state)
        {
            if (ServiceStatus != ServiceStatus.Authenticated && GameSettings.Player != null && GameSettings.Player.Id > 0) ServiceStatus = ServiceStatus.Authenticated;

            try
            {
                Send(string.Empty);
            }
            catch (Exception)
            {
                ServiceStatus = ServiceStatus.Disconnected;
            }
        }

        public async Task<bool> ConnectAsync(string connectionString)
        {
            if (ServiceStatus != ServiceStatus.Disconnected) throw new Exception("Status of service must be disconnected!");

            ServiceStatus = ServiceStatus.Connecting;

            _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            var connection = connectionString.Split(';');

            _endPoint = new IPEndPoint(IPAddress.Parse(connection[0]), int.Parse(connection[1]));

            try
            {
                await _serverSocket.ConnectAsync(_endPoint);
            }
            catch (Exception e)
            {
                ServiceStatus = ServiceStatus.Disconnected;

                throw e;
            }

            if (_checkStatusTimer != null) _checkStatusTimer.Dispose();

            _checkStatusTimer = new Timer(checkStatusCallback, null, 0, CHECK_STATUS_INTERVAL);

            ServiceStatus = ServiceStatus.Connected;

            var receiver = new Thread(beginReceive);

            receiver.Start();

            return true;
        }

        private void beginReceive()
        {
            var response = new Response();
            var body = string.Empty;
            var received = 0;
            var totalResponseLength = 0;
            var request = string.Empty;
            EntityType entityType = EntityType.Unknown;
            RequestType requestType = RequestType.Unknown;
            ResultType resultType = ResultType.Unknown;

            while (_serverSocket.Connected)
            {
                var buffer = new byte[MAX_RESPONSE_LENGTH];

                try
                {
                    // while buffer do not containt at least 6 elements => received
                    while (request.Length < 6)
                    {
                        received += _serverSocket.Receive(buffer);
                        request = Encoding.ASCII.ReadString(buffer);
                    }

                    if (EntityTypeExtension.TryParse(request[0], out entityType)) // Entity type
                    {
                        if (RequestTypeExtension.TryParse(request[1], out requestType)) // Request type
                        {
                            if (ResultTypeExtension.TryParse(request[2], out resultType)) // Result
                            {
                                if (int.TryParse(request.Substring(3, 3), out int lenght)) // Length
                                {
                                    totalResponseLength = (lenght + 6);

                                    // wait until whole request received 
                                    while (received < totalResponseLength)
                                    {
                                        received += _serverSocket.Receive(buffer);
                                        request += Encoding.ASCII.ReadString(buffer);
                                    }

                                    body = lenght > 0 ? request.Substring(6, lenght) : $"Length of { requestType } { entityType } response BODY must be positive integer!";
                                }
                                else
                                {
                                    totalResponseLength = 6;
                                    body = $"Length of { requestType } { entityType } response is in wrong format!";
                                }
                            }
                            else
                            {
                                totalResponseLength = 3;
                                body = $"Result type of { requestType } { entityType } response is in wrong format!";
                            }
                        }
                        else
                        {
                            totalResponseLength = 2;
                            body = $"Response type of entity { entityType } is in wrong format!";
                        }
                    }
                    else
                    {
                        totalResponseLength = 1;
                        body = "Response entity type is in wrong format!";
                    }

                    // remove response from buffer
                    request = request.Length == totalResponseLength ? string.Empty : request.Substring(totalResponseLength);
                    received -= totalResponseLength;

                    OnReceiveEvent?.Invoke(new Response
                    {
                        EntityType = entityType,
                        RequestType = requestType,
                        ResultType = resultType,
                        Body = body
                    });
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.StackTrace);

                    OnReceiveEvent?.Invoke(new Response
                    {
                        EntityType = entityType,
                        RequestType = requestType,
                        ResultType = ResultType.Error,
                        Body = $"Exception in Receiver thread! [{ e.Message }]"
                    });
                }
            }
        }

        public void Authenticate()
        {
            ServiceStatus = ServiceStatus.Authenticating;

            Send($"CC{ GameSettings.PlayerName }");
        }

        public void Disconnect()
        {
            if (_serverSocket.Connected) _serverSocket.Disconnect(true);

            _serverSocket.Dispose();

            if (GameSettings.Player != null) GameSettings.Player.Id = 0;

            ServiceStatus = ServiceStatus.Disconnected;
        }

        public void Send(string request)
        {
            _sendingMutex.WaitOne();

            if (request.Length <= MAX_REQUEST_LENGTH)
            {
                while (request.Length < MAX_REQUEST_LENGTH) request += "*";

                try
                {
                    var result = _serverSocket.Send(Encoding.ASCII.GetBytes(request));

                    if (result < 1) throw new Exception($"{ ResultType.Error };Nothing was send!");
                }
                catch (Exception e)
                {
                    _sendingMutex.ReleaseMutex();

                    throw e;
                }
            }

            _sendingMutex.ReleaseMutex();
        }

        public void Dispose()
        {
            _serverSocket.Close();
        }
    }
}
