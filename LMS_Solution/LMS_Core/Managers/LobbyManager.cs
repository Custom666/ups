﻿using LMS_Core.Enums;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using LMS_Core.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace LMS_Core.Managers
{
    public class LobbyManager : IManager<Lobby>
    {
        public const int MAX_LOBBY_NAME_LENGTH = 13;

        public ObservableCollection<Lobby> Items { get; }

        private readonly IService _service;
        private readonly IManager<NetworkClient> _clientManager;

        public LobbyManager(IService service, IManager<NetworkClient> clientManager)
        {
            _service = service;
            _clientManager = clientManager;

            Items = new ObservableCollection<Lobby>();

            NetworkService.OnReceiveEvent += NetworkService_OnReceiveEvent;
            NetworkService.OnServiceStatusChangedEvent += NetworkService_OnServiceStatusChangedEvent;
        }

        private void NetworkService_OnServiceStatusChangedEvent()
        {
            if (_service.ServiceStatus == ServiceStatus.Disconnected) Items.Clear();
        }

        private void NetworkService_OnReceiveEvent(IResponse response)
        {
            if (response.ResultType != ResultType.Ok || response.EntityType != EntityType.Lobby) return;

            if (string.IsNullOrEmpty(response.Body)) throw new Exception("Response body cannot be empty!");

            switch (response.RequestType)
            {
                case RequestType.Synchronize:

                    synchronize(response.Body.Split(';'));

                    break;
                case RequestType.GetAll:

                    synchronize(response.Body.Split(';'));

                    break;
                case RequestType.Create:

                    synchronize(response.Body.Split(';'));

                    break;
                case RequestType.Join:

                    var parameters = response.Body.Split(';');

                    if (!int.TryParse(parameters[0], out var id) || id < 0) throw new Exception("Lobby <id> must be positive integer!");

                    var lobby = Items.FirstOrDefault(i => i.Id == id);

                    if (lobby == null) throw new Exception("Lobby not found! Please make sure that lobbies are synchroized with server.");

                    lobby.Clients.Add(GameSettings.Player);

                    break;
                case RequestType.Leave:
                    
                    if (!int.TryParse(response.Body, out id) || id < 0) throw new Exception("Lobby <id> must be positive integer!");

                    lobby = Items.FirstOrDefault(i => i.Id == id);

                    if (lobby == null) throw new Exception("Lobby not found! Please make sure that lobbies are synchroized with server.");

                    if (lobby.Owner.Equals(GameSettings.Player)) Items.Remove(lobby); // destroy lobby
                    else lobby.Clients.Remove(GameSettings.Player); // leave lobby

                    break;
                case RequestType.Destroy:

                    if (!int.TryParse(response.Body, out id) || id < 0) throw new Exception("Lobby <id> must be positive integer!");

                    lobby = Items.FirstOrDefault(i => i.Id == id);

                    if (lobby == null) throw new Exception("Lobby not found! Please make sure that lobbies are synchroized with server.");

                    Items.Remove(lobby);

                    break;
            }
        }

        private void synchronize(string[] parameters)
        {
            if (parameters.Length < 5)
                throw new Exception("Synchronization lobby has wrong parameters count! <id><name><owner><capacity><actua capacity><list of clients size of actual capacity>");

            lock (Items)
            {
                if (!int.TryParse(parameters[0], out var id) || id < 1 || id > 255)
                    throw new Exception("Lobby <id> must be integer between <1; 255>!");

                var name = parameters[1];

                if (string.IsNullOrEmpty(name))
                    throw new Exception("Lobby <name> cannot be empty!");
                else if (name.Length > MAX_LOBBY_NAME_LENGTH)
                    throw new Exception($"Lobby <name> cannot be longer then { MAX_LOBBY_NAME_LENGTH }!");

                var ownerId = parameters[2];

                if (string.IsNullOrEmpty(ownerId))
                    throw new Exception("Lobby <owner> cannot be empty!");

                if (!int.TryParse(parameters[3], out var capacity) || capacity < 0 || capacity > 9)
                    throw new Exception("Lobby <capacity> must be integer between <0; 9>!");

                if (!int.TryParse(parameters[4], out var actualCapacity) || actualCapacity < 0 || actualCapacity > capacity)
                    throw new Exception($"Lobby <actual capacity> must be integer between <0; { capacity }>!");

                var existClient = _clientManager.Items.FirstOrDefault(c => c.Name == ownerId);

                var owner = existClient ?? (GameSettings.Player?.Name == ownerId ? GameSettings.Player : null);

                if (owner == null) throw new Exception($"Cannot found owner of lobby { id }! Please make sure that clients are synchronized with server.");

                var lobby = Items.FirstOrDefault(item => item.Id == id);

                if (lobby == null) // create new lobby
                {
                    lobby = new Lobby(id, name, capacity, owner);
                    Items.Add(lobby);
                }
                else lobby.Clients.Clear(); // synchronize clients 

                for (var i = 0; i < actualCapacity; i++)
                {
                    var clientName = parameters[5 + i];

                    var client = _clientManager.Items.FirstOrDefault(item => item.Name.CompareTo(clientName) == 0) ?? (GameSettings.Player.Name.CompareTo(clientName) == 0 ? GameSettings.Player : null);

                    if (client == null) throw new Exception($"Cannot found client { clientName }! Please make sure that clients are synchronized with server.");

                    lobby.Clients.Add(client);
                }
            }
        }

        public bool Set(Lobby poco, RequestType type)
        {
            switch (type)
            {
                case RequestType.Create: return createLobby(poco);
                case RequestType.Join: return joinLobby(poco);
                case RequestType.Leave: return leaveLobby(poco);
            }

            return false;
        }

        private bool leaveLobby(Lobby lobby)
        {
            try
            {
                _service.Send($"L{ RequestTypeExtension.ToMessageFormat(RequestType.Leave) }");
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private bool joinLobby(Lobby lobby)
        {
            try
            {
                _service.Send($"L{ RequestTypeExtension.ToMessageFormat(RequestType.Join) }{ lobby.Id }");
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private bool createLobby(Lobby lobby)
        {
            try
            {
                _service.Send($"L{ RequestTypeExtension.ToMessageFormat(RequestType.Create) }{ lobby.Capacity }{ lobby.Name }");
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
