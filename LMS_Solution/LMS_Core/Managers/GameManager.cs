﻿using LMS_Core.Enums;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using LMS_Core.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;

namespace LMS_Core.Managers
{
    public class GameManager : IManager<Game>
    {
        public delegate void OnGameChanged();
        public event OnGameChanged OnGameChangedEvent;

        public delegate void OnClientFire(NetworkClient client, float velX, float velY, float velZ);
        public static event OnClientFire OnClientFireEvent;

        public ObservableCollection<Game> Items { get; }

        private readonly IService _service;
        private readonly IManager<Lobby> _lobbyManager;
        private readonly IManager<NetworkClient> _clientManager;

        public GameManager(IService service, IManager<Lobby> lobbyManager, IManager<NetworkClient> clientManager)
        {
            _service = service;
            _lobbyManager = lobbyManager;
            _clientManager = clientManager;
            Items = new ObservableCollection<Game>();

            NetworkService.OnReceiveEvent += NetworkService_OnReceiveEvent;
            NetworkService.OnServiceStatusChangedEvent += NetworkService_OnServiceStatusChangedEvent;
        }

        private void NetworkService_OnServiceStatusChangedEvent()
        {
            if (_service.ServiceStatus == ServiceStatus.Disconnected) if (Items.Count > 0) lock (Items) Items.Clear();
        }

        private void NetworkService_OnReceiveEvent(IResponse response)
        {
            if (response.ResultType != ResultType.Ok || response.EntityType != EntityType.Game) return;

            switch (response.RequestType)
            {
                case RequestType.GetAll:

                    create(response.Body.Split(';'));

                    break;

                case RequestType.Synchronize:

                    synchronize(response.Body.Split(';'));

                    break;

                case RequestType.Create:

                    create(response.Body.Split(';'));

                    break;

                case RequestType.Fire:

                    var parameters = response.Body.Split(';');

                    if (parameters.Length != 4)
                        throw new Exception("Game fire synchronization has wrong parameters count! [<client name><x-velocity><y-velocity><z-velocity>]");

                    if (string.IsNullOrEmpty(parameters[0]))
                        throw new Exception("Game <client name> cannot be empty!");

                    if (!int.TryParse(parameters[1], out var velX))
                        throw new Exception("Game <x-velocity> must be integer!");

                    if (!int.TryParse(parameters[2], out var velY))
                        throw new Exception("Game <y-velocity> must be integer!");

                    if (!int.TryParse(parameters[3], out var velZ))
                        throw new Exception($"Game <z-velocity> must be integer!");

                    if (GameSettings.Player.Name.CompareTo(parameters[0]) == 0) return;

                    var client = _clientManager.Items.FirstOrDefault(c => c.Name.CompareTo(parameters[0]) == 0);

                    if (client == null) throw new Exception($"Game fire sync failed! Client not found!");

                    OnClientFireEvent?.Invoke(client, velX, velY, velZ);

                    break;

                case RequestType.Destroy:

                    if (string.IsNullOrEmpty(response.Body))
                        throw new Exception("Game <client name> cannot be empty!");

                    client = _clientManager.Items.FirstOrDefault(c => c.Name.CompareTo(response.Body) == 0) ?? (GameSettings.Player.Name.CompareTo(response.Body) == 0 ? GameSettings.Player : null);

                    if (client == null) throw new Exception($"Game client death sync failed! Client not found!");

                    var game = Items.FirstOrDefault(g => g.Clients.Any(c => c.Name.CompareTo(client.Name) == 0));

                    if (game == null) throw new Exception($"Game client death failed! Game not found!");

                    if (client.Equals(GameSettings.Player))
                    {
                        Items.Remove(game);
                    }
                    else
                    {
                        game.Clients.Remove(client);
                    }

                    break;
            }
        }

        private void create(string[] parameters)
        {
            if (parameters.Length < 8)
                throw new Exception("Game synchronization has wrong parameters count! <id><capacity><actua capacity><list of clients with their positions and health>");

            if (!int.TryParse(parameters[0], out var id) || id < 1 || id > 255)
                throw new Exception($"Game <id> must be integer between <1; 255>! [{ parameters[0] }]");

            if (!int.TryParse(parameters[1], out var capacity) || capacity < 0 || capacity > 9)
                throw new Exception($"Game <capacity> must be integer between <0; 9>! [{ parameters[1] }]");

            if (!int.TryParse(parameters[2], out var actualCapacity) || actualCapacity < 0 || actualCapacity > capacity)
                throw new Exception($"Game <actual capacity> must be integer between <0; { capacity }>!  [{ parameters[2] }]");

            var game = Items.FirstOrDefault(item => item.Id == id);

            if (game == null) game = new Game(id, capacity);

            var lobby = _lobbyManager.Items.FirstOrDefault(l => l.Id == game.Id);

            if (lobby != null) _lobbyManager.Items.Remove(lobby);

            var clients = new List<NetworkClient>();

            for (var i = 0; i < actualCapacity; i++)
            {
                var clientNameIndex = 3 + (i * 5);

                if (clientNameIndex >= parameters.Length) throw new Exception($"Creating game PARSING ERROR!"); // should never hapenned

                var clientName = parameters[clientNameIndex];

                var client = _clientManager.Items.FirstOrDefault(item => item.Name == clientName) ?? (GameSettings.Player.Name.CompareTo(clientName) == 0 ? GameSettings.Player : null);

                if (client == null) throw new Exception($"Cannot found client { clientName }! Please make sure that clients are synchronized with server.");

                if (!float.TryParse(parameters[clientNameIndex + 1], NumberStyles.Any, CultureInfo.InvariantCulture, out var x)) throw new Exception($"Cannot parse { parameters[clientNameIndex + 1] } as x-position!");
                if (!float.TryParse(parameters[clientNameIndex + 2], NumberStyles.Any, CultureInfo.InvariantCulture, out var y)) throw new Exception($"Cannot parse { parameters[clientNameIndex + 2] } as y-position!");
                if (!float.TryParse(parameters[clientNameIndex + 3], NumberStyles.Any, CultureInfo.InvariantCulture, out var z)) throw new Exception($"Cannot parse { parameters[clientNameIndex + 3] } as z-position!");
                if (!int.TryParse(parameters[clientNameIndex + 4], out var health)) throw new Exception($"Cannot parse { parameters[clientNameIndex + 4] } as health!");

                client.PositionX = x;
                client.PositionY = y;
                client.PositionZ = z;

                client.Health = health;

                clients.Add(client);
            }

            if (clients.Any(c => c.Name.CompareTo(GameSettings.Player.Name) == 0 && c.Health > 0))
            {
                foreach (var c in clients) if (!game.Clients.Contains(c)) game.Clients.Add(c);

                if (!Items.Contains(game)) Items.Add(game);
            }
        }

        private void synchronize(string[] parameters)
        {
            if (parameters.Length != 7)
                throw new Exception("Game synchronization has wrong parameters count! [<id>;<client name>;<x-position>;<y-position>;<z-position>;<y-rotation>;<health>]");

            if (!int.TryParse(parameters[0], out var id) || id < 1 || id > 255)
                throw new Exception("Game <id> must be integer between <1; 255>!");

            var game = Items.FirstOrDefault(item => item.Id == id);

            if (game == null) throw new Exception($"Game with id { id } do not exist! Please make sure you are synchronized with server.");

            var clientName = parameters[1];

            var client = game.Clients.FirstOrDefault(item => item.Name == clientName);

            if (client.Equals(GameSettings.Player)) return;

            if (client == null) throw new Exception($"Cannot found client { clientName }! Please make sure that clients are synchronized with server.");

            if (!float.TryParse(parameters[2], NumberStyles.Any, CultureInfo.InvariantCulture, out var x)) throw new Exception($"Cannot parse { parameters[2] } as x-position!");
            if (!float.TryParse(parameters[3], NumberStyles.Any, CultureInfo.InvariantCulture, out var y)) throw new Exception($"Cannot parse { parameters[3] } as y-position!");
            if (!float.TryParse(parameters[4], NumberStyles.Any, CultureInfo.InvariantCulture, out var z)) throw new Exception($"Cannot parse { parameters[4] } as z-position!");
            if (!float.TryParse(parameters[5], NumberStyles.Any, CultureInfo.InvariantCulture, out var rotationY)) throw new Exception($"Cannot parse { parameters[5] } as y-rotation!");
            if (!int.TryParse(parameters[6], out var health)) throw new Exception($"Cannot parse { parameters[6] } as health!");

            client.PositionX = x;
            client.PositionY = y;
            client.PositionZ = z;
            client.RotationY = rotationY;
            client.Health = health;

            OnGameChangedEvent?.Invoke();
        }

        public bool Set(Game game, RequestType type)
        {
            switch (type)
            {
                case RequestType.Synchronize: return synchronizeGame(game);
            }

            return false;
        }

        private bool synchronizeGame(Game game)
        {
            try
            {
                _service.Send($"G{ RequestTypeExtension.ToMessageFormat(RequestType.Synchronize) }{ (int)GameSettings.Player.PositionX } { (int)GameSettings.Player.PositionY } { (int)GameSettings.Player.PositionZ } { (int)GameSettings.Player.RotationY } { GameSettings.Player.Health }");
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
