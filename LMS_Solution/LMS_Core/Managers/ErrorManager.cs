﻿using LMS_Core.Enums;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using LMS_Core.Services;
using System;
using System.Collections.ObjectModel;

namespace LMS_Core.Managers
{
    public class ErrorManager : IManager<Error>
    {
        public ObservableCollection<Error> Items { get; } // TODO potentional overflow

        public ErrorManager()
        {
            Items = new ObservableCollection<Error>();

            NetworkService.OnReceiveEvent += NetworkService_OnReceiveEvent;
        }

        private void NetworkService_OnReceiveEvent(IResponse response)
        {
            if (response.ResultType == ResultType.Ok) return;

            Items.Add(new Error
            {
                EntityType = response.EntityType,
                RequestType = response.RequestType,
                Message = response.Body
            });
        }

        public bool Set(Error poco, RequestType type)
        {
            throw new NotImplementedException();
        }
    }
}
