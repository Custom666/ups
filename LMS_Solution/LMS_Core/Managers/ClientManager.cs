﻿using LMS_Core.Enums;
using LMS_Core.Infrastructure;
using LMS_Core.Models;
using LMS_Core.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace LMS_Core.Managers
{
    public class ClientManager : IManager<NetworkClient>
    {
        public delegate void OnClindDisconnected(NetworkClient client);
        public static event OnClindDisconnected OnClindDisconnectedEvent;

        public delegate void OnClindConnected(NetworkClient client);
        public static event OnClindConnected OnClindConnectedEvent;
        
        public ObservableCollection<NetworkClient> Items { get; }

        private readonly IService _service;

        public ClientManager(IService service)
        {
            _service = service;

            Items = new ObservableCollection<NetworkClient>();

            NetworkService.OnReceiveEvent += NetworkService_OnReceiveEvent;
            NetworkService.OnServiceStatusChangedEvent += NetworkService_OnServiceStatusChangedEvent;
        }
        private void NetworkService_OnServiceStatusChangedEvent()
        {
            if (_service.ServiceStatus == ServiceStatus.Disconnected) foreach (var c in Items) c.Id = 0;
        }

        private void NetworkService_OnReceiveEvent(IResponse response)
        {
            if (response.ResultType != ResultType.Ok || response.EntityType != EntityType.Client) return;

            if (string.IsNullOrEmpty(response.Body)) throw new Exception("Response body cannot be empty!");

            switch (response.RequestType)
            {
                case RequestType.Create: // authentication

                    var parameters = response.Body.Split(';');

                    if(parameters.Length != 2) throw new Exception("Client authenticatoin failed! Wrong parameters count! [<socket id>;<client name>]");

                    if(!int.TryParse(parameters[0], out var socketId) || socketId <= 0) throw new Exception("Client authenticatoin failed! <socket id> must be positive integer.");

                    if(string.IsNullOrEmpty(parameters[1])) throw new Exception("Client authenticatoin failed! <client name> cannot be empty.");

                    GameSettings.Player = new NetworkClient(socketId, parameters[1]);
                    
                    break;
                case RequestType.Destroy:
                    
                    if (!int.TryParse(response.Body, out socketId) || socketId <= 0) throw new Exception("Client destroying failed! <socket id> must be positive integer.");

                    var existClient = Items.FirstOrDefault(c => c.Id == socketId);

                    if (existClient == null) throw new Exception("Cannot find client that is disconnecting! Please make sure that clients are synchronized with server.");

                    existClient.Id = 0;

                    OnClindDisconnectedEvent?.Invoke(existClient);

                    break;
                case RequestType.Synchronize:

                    synchronize(response.Body.Split(';'));

                    break;
                case RequestType.GetAll:

                    synchronize(response.Body.Split(';'));

                    break;
            }
        }

        private void synchronize(string[] parameters)
        {
            if (parameters.Length < 2) throw new Exception("Client synchronization failed! Wrong parameters count! [<socket id>;<client name>]");

            for(var index = 0; index < parameters.Length; index += 2)
            {
                if (!int.TryParse(parameters[index], out var socketId)) throw new Exception("Client synchronization failed! <socket id> must be integer.");

                if (string.IsNullOrEmpty(parameters[index + 1])) throw new Exception("Client synchronization failed! <client name> cannot be empty.");

                var existClient = Items.FirstOrDefault(item => item.Name.CompareTo(parameters[index + 1]) == 0);

                if (existClient != null)
                {
                    existClient.Id = socketId;
                }
                else
                {
                    existClient = new NetworkClient(socketId, parameters[index + 1]);
                    Items.Add(existClient);
                }

                OnClindConnectedEvent?.Invoke(existClient);
            }
        }
        public bool Set(NetworkClient poco, RequestType type)
        {
            //switch (type)
            //{
            //    case RequestType.Create: return setClientName();
            //}

            throw new NotImplementedException("You cannot set client data!");
        }
    }
}
