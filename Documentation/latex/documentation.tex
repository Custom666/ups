\documentclass[paper4a]{article}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage[IL2]{fontenc}
\usepackage{times}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{algorithm, algpseudocode}
\usepackage{listings}
\usepackage{cleveref}
\usepackage{dirtree}

\begin{document}

\begin{titlepage}

\raggedright

\includegraphics{fav.png}

\vspace*{\stretch{1.0}}

\center{\Huge{\textbf{Úvod do počítačových sítí }} }
\center{\Huge{\textbf{Semestrální práce}} }

\vspace*{\stretch{1.0}}

\begin{LARGE}

\raggedright {\today} \hfill  Vítek Poór

\end{LARGE}

\end{titlepage}

\tableofcontents{}

\thispagestyle{empty}

\newpage

\section{Zadání}

V rámci této práce budu popisovat návrh a řešení implementace počítačové hry pro více hráčů. Toto je pouze shrnutí kompletního zadání \cite{original} s vytyčením důležitostí.
\par
Server bude naprogramován v nízkoúrovňovém jazyce a bude řešen pod operačním systémem Linux. Klient bude vytvořen v objektově orientovaném jazyce nad operačními systémy Linux a Windows. Komunikace bude realizována textovým nešifrovaným protokolem. Součástí práce bude logování komunikace. 

\newpage

\section{Protokol}

Protokol je ryze textový. Formát zpráv požadavku a odpovědi z klienta na server je odlišný. Je to dáno délkou zpráv, jelikož požadavek na server nevyžaduje
proměnlivou délku a tudíž ho lze implementovat s fixní délkou. Kdežto odpověď serveru již (dle možné robustnosti hry) může obsahovat zpávy podstatně delší a tudíž musí být zachován mechanismus proměnlivé délky zprávy. Obecně přenášená zpráva obsahuje hlavičku a tělo. Jediný rozdíl je právě v hlavičce, kde při zprávě s proměnlivou délkou je na konec hlavičky připojena délka těla zpávy. Požadavky i odpovědi jsou validovány jak na straně serveru tak i na straně klienta. Na klientovy se chyba projevuje vytvoření nového záznamu typu Error a je možné k němu přistupovat přes příslušného správce. Server obsahuje logovací mechanismus, kam zaznamenává kromě komunikace také chyby. Validační chyby při požadavku na straně serveru jsou vraceny na klienta s tělem obsahující chybový výpis.

\subsection{Požadavek na server}

Jak již bylo popsáno výše, požadavek je fixní délky. Jenomže všechny možné požadavky na server z principu nemusí mít stejnou délku. Z tohoto důvodu je zbytek těla zprávy doplněn symbolem '*'. Hlavička je pro všechny požadavky stejně dlouhá a to dva znaky. Zbytek jsou data těla. Pokud tělo neobsahuje žádná data, bude vyplněno doplňujícím symbolem. Následující obrázek zachycuje strukturu zprávy posílaných z klienta na server.

\begin{figure}[!ht]
\centering
\includegraphics[width=.5\textwidth]{clientRequest.pdf}
\caption{Struktura kllientského požadavku}
\label{fig:clientRequest}
\end{figure} 

Typ entity a typ požadavku definují hlavičku zprávy, kde typ entity znamená nad jakou logickou oblastí bude požadavek prováděn. Typ entity je výčtovým typem nabývajících hodnot:

\begin{enumerate}
\item Unknown - \textbf{U} - neznámá entita
\item ClientType - \textbf{C} - entita reprezentující klienty
\item LobbyType - \textbf{L} - entita reprezentující hrací místnost
\item GameType - \textbf{G} - entita reprezentující hru
\end{enumerate}

Typ požadavku je taktéž výčtovým typem a charakterizuje akci, která má být prováděna. Možné definované hodnoty jsou:

\begin{enumerate}
\item Unknown
	\item GetAll - \textbf{A} - požadavek na získání všech zázamů
	\item Create - \textbf{C} - požadavek na vytvoření záznamu
	\item Join - \textbf{J} - požadavek na připojení se
	\item Leave - \textbf{L} - požadavek na odpojení se
	\item Start - \textbf{S} - požadavek na odstartování
	\item Destroy - \textbf{D} - požadavek na zrušení
	\item Fire - \textbf{F} - požadavek na střelu
	\item Synchronize - \textbf{S} - požadavek na synchronizaci
\end{enumerate}

Poslední součástí zprávy je její tělo, které obsahuje data, s kterými se spustí výsledná akce nad vybranou entitou.
\par
Velká písmena definována u výčtových typů jsou jejich označení. Tato označení jsou prakticky posílána ve zprávě a tvoří dohromady hlavičku. Dle typu požadavku se odvíjí délka těla zprávy.

Příklad vytvoření záznamu klienta:

\begin{figure}[!ht]
\centering
\includegraphics[width=.5\textwidth]{authenticationRequestExample.pdf}
\caption{Authentikace klienta s jménem 'JmenoKlienta' při fixní délce zprávy 16 Bytů.}
\label{fig:clientRequest}
\end{figure} 

\subsection{Odpověď serveru}

Odpověď serveru rozšiřuje hlavičku zprávy o typ výsledku požadavku. A to buď OK nebo ERROR, opět reprezentován jako výčtový typ nabývajích hodnot K pro OK výsledek a N pro ERROR výsledek. Navíc požadavek serveru je proměnlivé délky. Tato vlastnost se ve formátu zprávy projeví prodloužením hlavičky o velikost těla zprávy formátovanou jako číslo o 3 znacích. To znamená že výsledná hlavička bude délky 6 znaků oproti hlavičky požadavků klienta, která má délku pouze 2 znaky. Zároveň při akci, která jakýmkoliv způsobem ovlinňuje stav entit, server posílá brodcast patřičnému okruhu klientů o dané změne (neboli v kontextu toho řešení server synchronizuje stav entity). Broadcast zpráva je stejného formátu jako odpoveď klientovi na danou akci vštšinou pouze se změnou typu požadavku na Synchronize.

Následující obrázek zachycuje strukturu zprávy posílaných jako odpoveď/požadavek serveru na klienta.

\begin{figure}[!ht]
\centering
\includegraphics[width=.5\textwidth]{serverRequest.pdf}
\caption{Struktura odpovědi/požadavku serveru.}
\label{fig:serverRequest}
\end{figure} 

Příklad úspěšné odpovědi vytvoření záznamu klienta:

\begin{figure}[!ht]
\centering
\includegraphics[width=.5\textwidth]{authenticationResponseExample.pdf}
\caption{Authentikace klienta s jménem 'JmenoKlienta'.}
\label{fig:clientRequest}
\end{figure} 

\newpage

\section{Implementace}

Server je implementován v jazyce C/C++11. Klient je implementován nad frameworkem .NET v jazyce c\# za pomocí grafického enginu Unity. Jádro celého projektu (server (LMS\_Server) a jádro klienta (LMS\_Core)) je impelemntováno v řešení LMS\_Solution. Označením LMS je myšleno LastManStanding, což je pracovním pojemnováním projektu.

\subsection{Server}

Server je implementován pomocí IDE Microsoft Visual Studio 2017. Server je vytvořen jako Linuxový projekt s nastaveným překladem na vzdálený stroj. 
Toto nastavení je možné změnit ve vlastnostech projektu a nové připojení je možno vytvořit ve Visual Studiuu přes Cross-platform connection managera.
Při správném nastavení je tedy možné vyvíjet a ladit Linuxové aplikace na platformě Windows \cite{windowsLinuxProject}. Samozřejmě, že samotný adresář src se zdrojovými soubory je přenostitelný a spustitelný soubor lze tak být sestaven kdekoliv pomocí Makefilu.

Adresářová struktura serveru je následující:
\\
\dirtree{%
.1 LMS\_Server\DTcomment{C/C++ projekt reprezentující server}.
.2 bin\DTcomment{adresář se spustitelným souborem vytvořeným Makefilem}.
.2 lib\DTcomment{adresář s knihovny třetích stran}.
.3 spdlog\DTcomment{knihovna třetí strany řešící logování}.
.2 src\DTcomment{adresář se zdrojovými kódy}.
.3 BrodcastMessage.h\DTcomment{obsahuje definici struktury zprávy společně se seznamem příjemců}.
.3 Client.cpp\DTcomment{deklarace modelu klienta}.
.3 Client.h\DTcomment{definice modelu klienta}.
.3 ClientManager.cpp\DTcomment{deklarace správce klientů}.
.3 ClientManager.h\DTcomment{definice správce klientů}.
.3 Game.cpp\DTcomment{deklarace modelu hry}.
.3 Game.h\DTcomment{definice modelu hry}.
.3 GameManager.cpp\DTcomment{deklarace správce her}.
.3 GameManager.h\DTcomment{definice správce her}.
.3 Lobby.cpp\DTcomment{deklarace modelu herní místnosti}.
.3 Lobby.h\DTcomment{definice modelu herní místnosti}.
.3 LobbyManager.cpp\DTcomment{deklarace správce herních místností}.
.3 LobbyManager.h\DTcomment{definice správce herních místností}.
.3 Main.cpp\DTcomment{vstupní bod programu. Validuje vstupní argumenty a spouští Server}.
.3 ManagerBase.h\DTcomment{abstraktní třída reprezentující správce}.
.3 Request.cpp\DTcomment{deklarace modelu požadavku}.
.3 Request.h\DTcomment{definice modelu požadavku}.
.3 Server.cpp\DTcomment{deklarace modelu Serveru}.
.3 Server.h\DTcomment{definice modelu Serveru. Server je zodovědný za životní cyklus připojeného klienta}.
.2 LMS\_Server.vcxproj\DTcomment{popis projektu Visual Studia}.
.2 Makefile\DTcomment{soubor pravidel pro překlad tohoto projektu}.
}

\subsection{Klient}

Klient je rozdělen do dvou projektů. Jádro klienta LMS\_Core je .NET Standard class library projekt a obsahuje logiku navázání spojení a implementovaný mechanismus komunikace pomocí výše definovaného protokolu. Dalším projektem je grafické uživatelské rozhraní vytvořené za pomoci grafického enginu Unity. Je to samostatný projekt mimo LMS\_Solution s názvem LMS\_Prototype. Název je odvozen od metodiky vývoje herního software pomocí prototypování.

\subsubsection{Jádro}

Jádro, jakožto knihovna tříd, je přeložitelné pomocí standartních nástrojů doručovaných společně s IDE Microsoft Visual Studio (nebo jakýmkoliv jiným nástrojem podporující sestavení .NET Standard class library projektu). Adresářová struktura ctí výchozí nastavení projetků vyvořených MS Visual Studiem.
\\
Struktura jmenného prostoru:
\\
\dirtree{%
.1 LMS\_Core\DTcomment{rodičovský prvek představující jádro klienta}.
.2 GameSettings.cs\DTcomment{nastavení hry. Obahuje především model právě přihlášeného klienta}.
.2 Enums\DTcomment{prostor s výčtovými typy}.
.3 EntityType.cs\DTcomment{typ entity}.
.3 ResponseResultType.cs\DTcomment{typ výsledku odpovědi}.
.3 ResponseType.cs\DTcomment{typ požadavku}.
.3 ServiceStatus.cs\DTcomment{status připojení}.
.2 Extension\DTcomment{možná rozšíření}.
.3 EncodingExtension.cs\DTcomment{rozšíření .NET třídy Encoding o převod bytů na řetězec  bez prázdných znaků}.
.2 Infrastructure\DTcomment{infrastruktura klientského projektu}.
.3 IManager.cs\DTcomment{rozhraní definující základní operace správců}.
.3 IPOCO.cs\DTcomment{začkovací rozhraní modelů}.
.3 IResponse.cs\DTcomment{rozhraní definující základní strukturu modelu odpovědi}.
.3 IService.cs\DTcomment{rozhraní definující základní operace servisů (připojení, odpojení, status...)}.
.3 POCO.cs\DTcomment{abstraktní třída definující strukturu použitých modelů}.
.2 Managers\DTcomment{prostor obsahující správce}.
.3 ClientManager.cs\DTcomment{správce klientů}.
.3 ErrorManager.cs\DTcomment{správce chyb. Jako chyba je považována odpověd s výsledkem ERROR}.
.3 GameManager.cs\DTcomment{správce hry}.
.3 LobbyManager.cs\DTcomment{správce herních místností}.
.2 Models\DTcomment{prostor obsahující modely}.
.3 Error.cs\DTcomment{model chyby}.
.3 Game.cs\DTcomment{model hry}.
.3 Lobby.cs\DTcomment{model herní místnosti}.
.3 NetworkClient.cs\DTcomment{model klienta}.
.3 Response.cs\DTcomment{model odpovědi serveru}.
.2 Services\DTcomment{prostor obsahující servis pro komunikaci s jinou aplikací}.
.3 NetworkService.cs\DTcomment{service definující spojení a komunikaci s aplikací pomocí TCP}.
}

\subsubsection{Grafické uživatelské rozhraní}

Výsledná klientská část je vypracována v Unity 2018.2.1f1. Jelikož jádro klienta vyžaduje pro správný chod spoustu závislostí, je použita knihovna třetí strany Zenject. Zenject je správce závislostí pro Unity projekty. Dovoluje vytvoření kontextu, který je přenositelný mezi scény a tím umožňuje nepřerušené spojení se serverem při změně menu scény na herní scénu. Dále pro lepší uživatelský zážitek byl integrován projekt Tanks! Tutorial \cite{tanksTutorial}, který vydalo Unity na svém obchodě Assets Store pro volné užití.
\par
Adresářová struktura ctí natavení Unity a navíc je přidán adresář Solution, který obsahuje verzi hry jako přeložitelný soubor řešení pro MS Visual Studio. Ze struktury je jediný důležitý adresář Assets, který obsahuje všechny uživatelem definované objekty. Skripty jsou členěny dle scén. Struktura jmenných prostorů je následující:
\\
\dirtree{%
.1 Assets\DTcomment{dle Unity prostor hráče, neboli vlastní tvorba na projektu}.
.2 Player.Scripts\DTcomment{skripty obsahující logiku hráčů ve hře, tj. klient a protihráči}.
.3 EnemyController.cs\DTcomment{skript obsahující logiku nastavení jednotlivého protihráče dle modelu}.
.3 PlayerController.cs\DTcomment{skript obsahující logiku nastavení modelu dle klienta jako hráče}.
.2 Scenes\DTcomment{skripty obsahující veškerou logiku herních scén}.
.3 GameScene.Scripts\DTcomment{skripty obsahující logiku scény hry}.
.4 GameUI.cs\DTcomment{grafické textové rozhraní herná scény}.
.4 Controllers\DTcomment{scripty zajišťující chod scény}.
.5 GameController.cs\DTcomment{Zajišťuje herní synchronizaci hráče}.
.3 MenuScene.Scripts\DTcomment{skripty obsahující logiku menu scény}.
.4 UIManager.cs\DTcomment{správce menu scény. Inicializuje aplikaci a obsahuje metody dostupné v menu}.
.4 GameCreator.cs\DTcomment{stará se o načtení herní scény, pokud je to možné}.
.4 Controllers\DTcomment{scripty zajišťující chod scény}.
.5 CreateLobbyDialogController.cs\DTcomment{obsahuje logiku dialogu pro tvorbu nového lobby}.
.5 LobbyScreenController.cs\DTcomment{obsahuje logiku obrazu s herní místností}.
.5 MultiplayerScreenController\DTcomment{obsahuje logiku obrazu s herními místnostmi}.
.4 ViewModels\DTcomment{scripty zajišťující chod scény}.
.5 ClientLayoutElementViewModel.cs\DTcomment{model s daty klienta zobrazeného jako položka seznamu}.
.5 LobbyLayoutElementViewModel.cs\DTcomment{model s daty herní místnosti zobrazené jako položka seznamu}.
.2 Scripts.Installers\DTcomment{obsahuje tzv. installery, které slouží pro inicializaci Zenject kontextu}.
.3 NetworkInstaller.cs\DTcomment{zavádí binding rozhraní na konkrétní implementace pro správu závislostí}.
.1 Complete\DTcomment{výchozí prostor pro integrovaný projekt Tank! Tutorial. Obsahuje logiku uživatelské interakce s tankem. Jelikož je kód velice dobře dokumentován, je popis struktury z dokumentace vynechán.}.
}

\newpage

\section{Uživatelská příručka}

Hra by se měla spouštět z již vytvořených spustitelných souborů v adresáři Release. Adresář obsahuje přeloženého klienta na všechny verze platform Windows a Linux. Server lze přeložit souborem Makefile pouze v linuxovém prostředí. 

\subsection{Překlad klienta}

Ruční překlad klienta lze nejjednodušším způsobem přes Unity verze 2018.2.1f1 a vyšší. Tento způsob je doporučován, jelikož si můžete zvolit libovolné parametry překladu a výsledkem bude "release" verze projektu.
\par
Dalším způsobem je překlad řešení v MS Visual Studiu, které je k nalezení v adresáři LMS\_Prototype/Solution. Jedná se o "development" verzi projektu získanou ze specifického nastavení překladu v prostředí Unity.
\par
Nutno dodat, že výše uvedné postupy zahrnují překlad pouze Unity projektu. Samostatné jádro klienta je zde tedy přibaleno jako knihovna třetí strany a v žádném případě neprobíhá její vytvoření. Tudíž při potencionální upravě jádra klienta je zapotřebí přeložt řešení LMS\_Core. Toto řešení má ve vlastnostech nastavenou cestu sestavení do adresáře LMS\_Prototype/Assets/Plugins, což je adresář pro přidání knihoven třetích stran definován Unity. Z toho plyne, že pokud překládáme klienta z prostředí Unity, a potřebujeme aktuální verzi jádra, postačí nám pouze sestavit jádro ve Visual Studiu a poté řeložit klienta standardním způsobem v Unity. Pokud ovšem překládáme klienta jiným způsobem, je zapotřebí zvlášť přeložit jádro a poté ho nakopírovat do adresáře LMS\_Prototype\_Data/Managed, který je definován Unity, jako adresář s knihovny potřebných pro chod konkrétní sestavené hry.

\subsection{Argumenty}

Argumenty u klienta nebyly implementovány. Naopak Server vyžaduje spuštění alespoň s jedním parametrem ze tří možných:

\begin{itemize}

\item první parametr je povinný \[c\] nebo \[f\]. Určuje zdali bude logování prováděno do konzole nebo do souboru.
\item druhý parametr je nepovinný \[ip adresa\]. Pokud nebude definován, použije se 127.0.0.1.
\item třetí parametr je nepovinný \[port\]. Pokud nebude definován, použije se 10000.

\end{itemize}

\newpage

\section{Závěr}

Jelikož projekt prošel pouze akceptačním testováním, je možný vznik nečekaných chyb. Nicméne i přes to slouží jako demonstrace funkčnosti navrženého protokolu a zajištení komunikace pomocí architektury klient-server. Taktéž různorodost řešení, co se typů projektů týče, je rozsáhlá a lze tak projekt vcelku libovolně a jednoduše upravit. Při implementaci řešení byl kladen důraz na obecnost a nezávislost. Samotné jádro klienta by mělo být schopno produkovat stejné výsledky s jakýmkoliv jiným uživatelským rozhraním. 
\par
Možná změna by mohla řešit synchronizaci střel a pohybu. V současné implementaci je synchronizace pohybu z časových důvodů řešena vektorem pozice a posíláním jeho změny při překročení definovaného práhu. Synchronizace střely je řešena pouze informací kdo a kde vystřelil. Samotný střet s protihráčem je řešen až na straně klienta, což je značně nepraktické řešení. I přesto bylo vybráno, jelikož je snadné na implementaci. Optimálním řešením by bylo zavedení časových značek požadavků a dočasné ukládání. Tímto způsobem je možné na straně serveru sestavit herní svět v momentě výstřelu a matematicky dopočítat v čase trajektorii střely a potencionální kolizi s hráči. Serveru by se tak navrátila autoritativa, která mu náleží.
\par
Jelikož se tento projekt zabýval pouze technickou stánkou problému, nebyl kladen důraz na grafické uživatelské rozhraní. Byla by tedy potřebná úprava vlastností zobrazených protihráčů a menu ve hře, pro lepší uživatelský zážitek.

\newpage

\begin{thebibliography}{9}

\bibitem{original} 

\textit{Semestrální práce z UPS 2018 - hra pro více hrářů}. [http://home.zcu.cz/\~ublm/vyuka/ups/KIV\_UPS\_2018\_POZADAVKY.pdf]

\bibitem{windowsLinuxProject} 

\textit{Konfigurace projektu Linux v MS Visual Studio 2017} [https://docs.microsoft.com/cs-cz/cpp/linux/configure-a-linux-project?view=vs-2017]

\bibitem{tanksTutorial} 

\textit{Unity projekt Tanks! Tutorial}. [https://assetstore.unity.com/packages/essentials/tutorial-projects/tanks-tutorial-46209]

\end{thebibliography}

\end{document}